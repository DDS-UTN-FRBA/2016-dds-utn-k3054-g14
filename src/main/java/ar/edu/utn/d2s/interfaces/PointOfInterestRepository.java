package ar.edu.utn.d2s.interfaces;

import java.util.List;

import ar.edu.utn.d2s.model.poi.PointOfInterest;

public interface PointOfInterestRepository extends Searcher {
	
	/**
	 * Add a POI to the collection
	 * 
	 * @param point
	 */
	public void add(PointOfInterest point);
	
	/**
	 * Returns the total count of POIs
	 * 
	 * @return
	 */
	public int count();
	
	/**
	 * Delete a point of interest if existent
	 * 
	 * @param point
	 */
	public void remove(PointOfInterest point);
	
	/**
	 * Get all points of interest 
	 * 
	 *  
	 */
	public List<PointOfInterest> getAll();
	
	/**
	 * Get all Active points of interest 
	 * 
	 *  
	 */
	public List<PointOfInterest> getAllActives();

	/**
	 * Updates a points of interest 
	 * 
	 *  
	 */
	public void update(PointOfInterest poi);
	
}
