package ar.edu.utn.d2s.interfaces;

import java.util.List;

import ar.edu.utn.d2s.model.poi.PointOfInterest;

public interface Searcher {
	  
	/**
	 * Search points of interest by a set of keywords
	 * 
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	public List<PointOfInterest> search(String... keywords) throws Exception;
}

