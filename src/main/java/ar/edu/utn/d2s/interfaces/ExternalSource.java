package ar.edu.utn.d2s.interfaces;

import java.util.List;

import ar.edu.utn.d2s.model.poi.PointOfInterest;
 
public interface ExternalSource extends Searcher {

	/**
	 * Retrieve a list of points of interest from an external source
	 * 
	 * @return
	 * @throws Exception 
	 */
	public List<PointOfInterest> getPointsOfInterest() throws Exception;
}


