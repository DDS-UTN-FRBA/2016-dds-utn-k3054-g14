package ar.edu.utn.d2s.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext; 

import ar.edu.utn.d2s.hibernate.HibernateConfiguration;
import ar.edu.utn.d2s.interfaces.PointOfInterestRepository;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.model.poi.Shop;
import ar.edu.utn.d2s.services.PoiService;

 
public class InMemoryRepository implements PointOfInterestRepository {

	AbstractApplicationContext context = new AnnotationConfigApplicationContext(HibernateConfiguration.class);
	
	PoiService poiService= (PoiService) context.getBean("poiService");
		
	protected List<PointOfInterest> pointsOfInterest;	
	
	/** Constructors */
	
	
	public InMemoryRepository() {
		pointsOfInterest = new ArrayList<PointOfInterest>();
	}
	
	/** Private methods */

	private boolean contains(PointOfInterest poi) {

		if( this.pointsOfInterest.stream().anyMatch(aPoint -> aPoint.is(poi)))  
				return true;
		else 
			return false;
	}
	
	/** Implementations */

	@Override
	public void add(PointOfInterest poi) {
		if (!this.contains(poi)) {
			this.pointsOfInterest.add(poi);
			poiService.save(poi);
		}
	}

	@Override
	public int count() {
		int count1= this.pointsOfInterest.size();
		int count2= poiService.findAll().size();
		if(count1==count2)
			return count1;
		else
			return count2;
	}

	@Override
	public void remove(PointOfInterest poi) {
		this.poiService.delete(poi);
		this.pointsOfInterest.removeIf(aPoint -> aPoint.is(poi));
		
	}
	
	@Override
	public void update(PointOfInterest poi){
		
	 
	}

	@Override
	public List<PointOfInterest> search(String... keywords) {
		
		
		
		// Can only be called with 1 argument
		if (keywords.length != 1) {
			return null;
		}
		
		List<PointOfInterest> response = new ArrayList<PointOfInterest>();
		String keyword = keywords[0];
				
		response.addAll(
			this.poiService.findAll().stream()
			.filter(point -> point.matches(keyword))
			.collect(Collectors.toList())
		);
		
		
		
		return response;		
	}

	public List<PointOfInterest> searchLocal(String... keywords) {
		
		
		
		// Can only be called with 1 argument
		if (keywords.length != 1) {
			return null;
		}
		
		List<PointOfInterest> response = new ArrayList<PointOfInterest>();
		String keyword = keywords[0];
				
		response.addAll(
			this.pointsOfInterest.stream()
			.filter(point -> point.matches(keyword))
			.collect(Collectors.toList())
		);
		
		
		
		return response;		
	}
	
	@Override
	public List<PointOfInterest> getAll() {
		 
		return poiService.findAll();
	}
	

	public List<PointOfInterest> getAllLocal() {
		 
		return this.pointsOfInterest;
	}

	@Override
	public List<PointOfInterest> getAllActives() {
		 
		return this.pointsOfInterest.stream()
				.filter(point -> point.isActive())
				.collect(Collectors.toList());
	}

	public void addLocal(Shop newShop) {
		this.pointsOfInterest.add(newShop);
	}

	  
}
