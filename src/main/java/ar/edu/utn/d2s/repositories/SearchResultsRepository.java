package ar.edu.utn.d2s.repositories;

import java.util.ArrayList;
import java.util.List; 

import ar.edu.utn.d2s.model.Query;

public class SearchResultsRepository {

	protected List<Query> querysResults;
	/** Constructors */
	
	public SearchResultsRepository() {
		querysResults = new ArrayList<Query>();
	}
	
	/** Private methods */
	 
	
	/** Implementations */

 
	public void add(Query point) {
		
			this.querysResults.add(point);
	}

 
	public int count() {
		return this.querysResults.size();	
	}

	 
	public void remove(Query point) {
		this.querysResults.removeIf(aPoint -> aPoint.is(point));
	}
 
	public List<Query> getAllContent(){
		return this.querysResults;
	}
}
  
