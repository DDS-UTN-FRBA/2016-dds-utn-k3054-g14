package ar.edu.utn.d2s.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import ar.edu.utn.d2s.exceptions.user.UserNotFoundException;
import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.useractions.Action;

public class UsersRepository {

	protected List<User> users;
	protected List<User> lastStateOfUsers;
	/** Constructors */
	
	public UsersRepository() {
		users = new ArrayList<User>();
		lastStateOfUsers = new ArrayList<User>();
	}
	
	/** Private methods */
	
	public boolean contains(User user) {
		return this.users.stream().anyMatch(aUser -> aUser.is(user));
	
	}
	
	/** Implementations */

 
	public void add(User user) {
		if (!this.contains(user)) {
			this.users.add(user);
		}
	}
 
	public int count() {
		return this.users.size();	
	}
	 
	public void remove(User user) {
		this.users.removeIf(aUser -> aUser.is(user));
	}
	
	
	
	public void updateActions(User user, Set<Action> userActions) throws UserNotFoundException{
		if(this.contains(user)){
			
			for(Action action : userActions)
				user.addUserActions(action);
		}
		else
			throw new UserNotFoundException();
	}

	public void revertState() {
		 this.users.clear();
		 this.users.addAll(lastStateOfUsers);
	}
	
	
	//*** getters & setters**//
	
	
	public List<User> getAllContent(){
		return this.users;
	}

	public void setLastStateOfUsers(List<User> lastStateOfUsers) {
		this.lastStateOfUsers.clear();
		this.lastStateOfUsers = lastStateOfUsers;
	}

	public List<User> getAllContentOflastStateOfUsers(){
		return this.lastStateOfUsers;
	}


	
	
}
