package ar.edu.utn.d2s.repositories;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import ar.edu.utn.d2s.interfaces.ExternalSource;
import ar.edu.utn.d2s.interfaces.Searcher;
import ar.edu.utn.d2s.model.poi.PointOfInterest;

public class ExternalRepository implements Searcher {

	protected Hashtable<String, List<PointOfInterest>> cache;
	protected List<ExternalSource> externalSources;
	int hitsInCache;
	
	/** Constructors */
	
	public ExternalRepository(List<ExternalSource> sources) {
		this.cache = new Hashtable<String, List<PointOfInterest>>();
		this.hitsInCache = 0;
		
		this.externalSources = sources;
	}
	
	/** Implementations */

	@Override
	public List<PointOfInterest> search(String... keywords) throws Exception {
		// Accepts 1 or 2 arguments
		if (keywords.length != 1 && keywords.length != 2) {
			return null;
		}
		
		// First we check if we have the keywords already loaded in the cache.
		// If that's true, then we simply build the response using that info.
		String key = this.buildKey(keywords);
				
		if (this.cache.containsKey(key)) {
			this.hitsInCache++;
			return this.cache.get(key);
		}

		List<PointOfInterest> response = new ArrayList<PointOfInterest>();
				
		// loop every external source
		for (ExternalSource externalSource : this.externalSources) {
			response.addAll(externalSource.search(keywords));
		}
		
		// before returning the results, add them to the cache
		this.cache.put(key, response);
				
		return response;	
	}

	/** Private methods */
	
	/**
	 * Build a unique key from an array of strings
	 * 
	 * If the array has just one string, that string will be the key
	 * If it has two strings, we implode the array separating them with a dot .
	 * 
	 * @param keywords
	 * @return
	 */
	private String buildKey(String[] keywords) {
		if (keywords.length == 1) {
			return keywords[0].replaceAll("\\s","").toLowerCase();
		}
		
		return 	keywords[0].replaceAll("\\s","").toLowerCase() + 
				"." + 
				keywords[1].replaceAll("\\s","").toLowerCase();
	}
	
	/** Getters & Setters */
	
	public int getHitsInCache() {
		return this.hitsInCache;
	}
}
