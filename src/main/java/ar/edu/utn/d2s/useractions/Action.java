package ar.edu.utn.d2s.useractions;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.joda.time.DateTime;

import ar.edu.utn.d2s.exceptions.user.UserCanNotReceiveEmailNotifException;
import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.model.poi.PointOfInterest;



@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="action", discriminatorType=DiscriminatorType.STRING)
public abstract class Action {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int action_id;
	
	@Column(name = "name")
	@NotNull
	@Size(min = 3, max = 60, message = "A User's name can't have more than 60 characters")
	@Pattern(regexp = "[a-zA-Z]*", message = "A User's name must have only letters")
	String actionName;
	
	@Column(name = "isActive")
	@NotNull
	public Boolean isActive;

	public Action(String actionName) {
		super();
		this.actionName = actionName;
		this.isActive=true; 
	}

	
	public Action() {
		super();
	}


	public void action(String keyword, List<PointOfInterest> pdis,

		DateTime initDateTime, DateTime endDateTime, User user) throws UserCanNotReceiveEmailNotifException {
		
		
	}
	
	public void activateAction(){
		this.isActive=true;
	}
	
	public void deactivateAction(){
		this.isActive=false;
	}
		
	public Boolean isActive(){
		return this.isActive;
	}
	
}
