package ar.edu.utn.d2s.useractions;
 
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.joda.time.DateTime;

import ar.edu.utn.d2s.model.Query;
import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.repositories.SearchResultsRepository;

@Entity
@DiscriminatorValue(value="GenerateSearchReport")
public class GenerateSearchReport extends Action
{
	@Transient
	SearchResultsRepository globalSearchesRepo;
	
	public GenerateSearchReport(String actionName ) {
		super(actionName);
		 
	}
	
	public GenerateSearchReport(    ) {
		super( );
		 
	}
	//**methods**/
	
	@Override
	public void action(String keyword, List<PointOfInterest> PDIlist, DateTime initDateTime, DateTime endDateTime, User user) {
	
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String reportDate = df.format(initDateTime.toDate());
	
		Query query = new Query();
		query.setDate(reportDate);
		query.setUser(user);
		query.setKeyword(keyword);
		Set<PointOfInterest> setPdis = new HashSet<PointOfInterest>(PDIlist);
		query.setResults(setPdis);
		query.setQtyResults(setPdis.size());
							
		//insert query in searchResultsRepository
		user.getSearchesRepo().add(query); 
	}

}

