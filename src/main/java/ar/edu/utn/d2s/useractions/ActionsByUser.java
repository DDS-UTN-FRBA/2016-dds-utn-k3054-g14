package ar.edu.utn.d2s.useractions;


import java.util.Set;

import ar.edu.utn.d2s.model.User;


public class ActionsByUser {
	
	private User user;
	private Set<Action> newActions;
		

	//**constructor**//
	
	public ActionsByUser(User user, Set<Action> newActions) {
		super();
		this.user = user;
		this.newActions = newActions;
	}
	
	
	//**setters & getters**//
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}

	public Set<Action> getNewActions() {
		return newActions;
	}
	
	public void setNewActions(Set<Action> newActions) {
		this.newActions = newActions;
	}


}
