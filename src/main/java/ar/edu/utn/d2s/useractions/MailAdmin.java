package ar.edu.utn.d2s.useractions;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;
import org.joda.time.Seconds;

import ar.edu.utn.d2s.exceptions.user.UserCanNotReceiveEmailNotifException;
import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
@Entity
@DiscriminatorValue(value="MailAdmin")
public class MailAdmin extends Action{

	@Transient
	private User admin = new User();
	@Column(name = "user_id")
	@NotNull
	private int user_id;
	public Long MAX_DELAY;
	
	public MailAdmin(String actionName){
		super(actionName);
		
	};
	
	public MailAdmin(   ){
		super( );
		
	};
	
	public MailAdmin(String actionName, String name, User admin, Boolean isActive, long max_delay) {
		super(actionName);
	 
		this.admin = admin;
		this.MAX_DELAY=max_delay;

		setUser_id(admin.getId());
	}


	//**methods **//
	
	@Override
	public void action(String keyword, List<PointOfInterest> pdis,
			DateTime initDateTime, DateTime endDateTime, User user) throws UserCanNotReceiveEmailNotifException {
		long delay = Seconds.secondsBetween(endDateTime, initDateTime).getSeconds();
		
		if(delay>MAX_DELAY){
			if(user.isAdmin())
				user.emailNotification();;//send email to admin				
		}
	}

	
	
	//**getters & setters **//
	
	
 

	public Long getMAX_DELAY() {
		return MAX_DELAY;
	}


	public void setMAX_DELAY(Long mAX_DELAY) {
		MAX_DELAY = mAX_DELAY;  //this value should be taken from a properties file 
	}

 
	public User getAdmin() {
		return admin;
	}

	public void setAdmin(User admin) {
		this.admin = admin;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	
	
}
