package ar.edu.utn.d2s.useractions;


import java.util.List;
import java.util.logging.Logger;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.joda.time.DateTime;
import org.joda.time.Seconds;

import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.model.poi.PointOfInterest;


@Entity
@DiscriminatorValue(value="GenerateLog")
public class GenerateLog extends Action{

	private final static Logger logger = Logger.getLogger(GenerateLog.class.getName());
	
	public GenerateLog(String actionName) {
		super(actionName);		
	}
	
	
	public GenerateLog(   ) {
		super( );		
	}
	//**methods**//

	@Override
	public void action(String keyword, List<PointOfInterest>pdis , DateTime initDateTime, DateTime endDateTime, User user) {
				
	 
		long totalSeekTime = Seconds.secondsBetween(initDateTime, endDateTime).getSeconds();
				
		
		logger.info("KeyWord: " + keyword);
		logger.info("Quantity: " + pdis.size());
		logger.info("SeekTime in seconds: " + totalSeekTime);
		
	}
	

	//**geters & setters **//

	public String getActionName() {
		return actionName;
	}


	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
		
	
}
