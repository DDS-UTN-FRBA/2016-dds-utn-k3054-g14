package ar.edu.utn.d2s.libraries;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.joda.time.LocalTime;

@Entity
public class TimeInterval {

	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@NotNull
	private int id;
	@Column(name = "startsAt") 
	private LocalTime startsAt;
	@Column(name = "endsAt") 
	private LocalTime endsAt;
	
	public TimeInterval(LocalTime startsAt, LocalTime endsAt) {
		this.setStartsAt(startsAt);
		this.setEndsAt(endsAt);
	}
		
	public TimeInterval(   ) {
	 
	}
	
	public TimeInterval(int startsAtHour, int startsAtMinutes, int endsAtHour, int endsAtMinutes) {
		LocalTime startsAt = new LocalTime(startsAtHour, startsAtMinutes);
		LocalTime endsAt = new LocalTime(endsAtHour, endsAtMinutes);
		
		this.setStartsAt(startsAt);
		this.setEndsAt(endsAt);
	}

	public TimeInterval(String startsAtString, String endsAtString) {
		String[] startsAtArray = startsAtString.split(":");
		String[] endsAtArray = endsAtString.split(":");
		
		int startsAtHour = Integer.parseInt(startsAtArray[0]);
		int startsAtMinutes = Integer.parseInt(startsAtArray[1]);
		int endsAtHour = Integer.parseInt(endsAtArray[0]);
		int endsAtMinutes = Integer.parseInt(endsAtArray[1]);
		
		LocalTime startsAt = new LocalTime(startsAtHour, startsAtMinutes);
		LocalTime endsAt = new LocalTime(endsAtHour, endsAtMinutes);
		
		this.setStartsAt(startsAt);
		this.setEndsAt(endsAt);
 	}
	
	/**
	 * Build a list with any amount of string intervals
	 * 
	 * @param timeIntervalStrings
	 * @return
	 */
	public static List<TimeInterval> list(String... timeIntervalStrings) {
		List<TimeInterval> list = new ArrayList<TimeInterval>();
		
		for (String timeIntervalString: timeIntervalStrings) { // "9:00 to 13:00"
			String[] intervalArray = timeIntervalString.split(" to "); // ["9:00", "13:00"]
			TimeInterval timeInterval = new TimeInterval(intervalArray[0], intervalArray[1]);
			list.add(timeInterval);
		}
		
		return list;
	}

	public LocalTime getEndsAt() {
		return endsAt;
	}

	public void setEndsAt(LocalTime time) {
		this.endsAt = time;
	}

	public LocalTime getStartsAt() {
		return startsAt;
	}

	public void setStartsAt(LocalTime time) {
		this.startsAt = time;
	}

	public boolean contains(LocalTime time) {
		return this.startsAt.isBefore(time) && this.endsAt.isAfter(time);
	}

}
