package ar.edu.utn.d2s.libraries.days;

import java.util.ArrayList;
import java.util.List;

public class DaysBuilder {
	
	private List<Integer> days;
	
	public DaysBuilder() {
		days = new ArrayList<Integer>();
	}

	public DaysBuilder addMonday() {
		this.add(Days.getMonday());
		
		return this;
	}
	
	public DaysBuilder addTuesday() {
		this.add(Days.getTuesday());
		
		return this;
	}
	
	public DaysBuilder addWednesday() {
		this.add(Days.getWednesday());
		
		return this;
	}
	
	public DaysBuilder addThursday() {
		this.add(Days.getThursday());
		
		return this;
	}
	
	public DaysBuilder addFriday() {
		this.add(Days.getFriday());
		
		return this;
	}
	
	public DaysBuilder addSaturday() {
		this.add(Days.getSaturday());
		
		return this;
	}
	
	public DaysBuilder addSunday() {
		this.add(Days.getSunday());
		
		return this;
	}
	
	/**
	 * Add day only if not already added to the list of days
	 * 
	 * @param day
	 */
	private void add(int day) {
		if (!getDays().contains(day)) {
			getDays().add(day);
		}
	}

	public List<Integer> getDays() {
		return days;
	}
	
	public List<Integer> get() {
		return getDays();
	}
}
