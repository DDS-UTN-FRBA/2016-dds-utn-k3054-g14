package ar.edu.utn.d2s.libraries.days;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.DateTimeConstants;

public class Days {
	
	private static int monday = DateTimeConstants.MONDAY;
	private static int tuesday = DateTimeConstants.TUESDAY;
	private static int wednesday = DateTimeConstants.WEDNESDAY;
	private static int thursday = DateTimeConstants.THURSDAY;
	private static int friday = DateTimeConstants.FRIDAY;
	private static int saturday = DateTimeConstants.SATURDAY;
	private static int sunday = DateTimeConstants.SUNDAY;
	
	private static DaysFactory factory = new DaysFactory();
	private static DaysBuilder builder;
	
	/** Factory methods */
		
	public static List<Integer> fromMondayToFriday() {
		return factory.fromMondayToFriday();
		
	}

	public static List<Integer> fromMondayToSaturday() {
		return factory.fromMondayToSaturday();
	}
	
	/** Builder methods */
	
	public static DaysBuilder addMonday() {
		return addDay("Monday");
	}
	
	public static DaysBuilder addTuesyday() {
		return addDay("Tuesday");
	}
	
	public static DaysBuilder addWednesday() {
		return addDay("Wednesday");
	}
	
	public static DaysBuilder addThursday() {
		return addDay("Thursday");
	}
	
	public static DaysBuilder addFriday() {
		return addDay("Friday");
	}
	
	public static DaysBuilder addSaturday() {
		return addDay("Saturday");
	}
	
	public static DaysBuilder addSunday() {
		return addDay("Sunday");
	}
	
	/**
	 * Proxy between the Day class and the builder using refletion
	 * 
	 * @param day
	 * @return
	 */
	private static DaysBuilder addDay(String day) {
		if (builder == null) {
			builder = new DaysBuilder();
		}
		
		// we call the method add{Day} in the DayBuilder object using reflection
		// we also wrap it in a try/catch block so that we don't have to deal with exceptions
		try {
			builder.getClass().getMethod("add"+day).invoke(builder);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			// something went wrong :(
			e.printStackTrace();
		}
		
		return builder;
	}
	
	public static List<Integer> get() {
		return builder.getDays();
	}
	
	/** Getters */

	public static int getMonday() {
		return monday;
	}

	public static int getTuesday() {
		return tuesday;
	}

	public static int getWednesday() {
		return wednesday;
	}
	
	public static int getThursday() {
		return thursday;
	}

	public static int getFriday() {
		return friday;
	}

	public static int getSaturday() {
		return saturday;
	}

	public static int getSunday() {
		return sunday;
	}
}
