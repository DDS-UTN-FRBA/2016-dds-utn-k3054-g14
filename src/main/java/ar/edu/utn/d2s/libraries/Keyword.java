package ar.edu.utn.d2s.libraries;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Keyword {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;
	
	@Column(name = "word")
	@NotNull	
	private String word;
	
	public Keyword(String word) {
		super();
		this.word = word;
	}
	public Keyword( ) {
		super();
		 
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

 
}
