package ar.edu.utn.d2s.libraries;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.joda.time.LocalTime;


@Entity
public class Schedule {
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@NotNull
	private int id;
	@Transient
	private List<Integer> days = new ArrayList<Integer>();
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<TimeInterval> timeIntervals = new ArrayList<TimeInterval>();
	
	/** Constructor */

	public Schedule(List<Integer> days, List<TimeInterval> timeIntervals) {
		this.setDays(days);
		this.setTimeIntervals(timeIntervals);
	}
	
	public Schedule( ) {}
	 
	
	/** Getters & Setters */

	public List<Integer> getDays() {
		return days;
	}

	public void setDays(List<Integer> days) {
		this.days = days;
	}

	public List<TimeInterval> getTimeIntervals() {
		return timeIntervals;
	}

	public void setTimeIntervals(List<TimeInterval> timeIntervals) {
		this.timeIntervals = timeIntervals;
	}
	
	/** Methods */

	public boolean contains(int day, LocalTime time) {
		
		if (!this.days.contains(day)) {
			return false;
		}
		
		for (TimeInterval timeInterval : this.timeIntervals) {
			if (timeInterval.contains(time)) {
				return true;
			}
		}
		
		return false;	
	}

}
