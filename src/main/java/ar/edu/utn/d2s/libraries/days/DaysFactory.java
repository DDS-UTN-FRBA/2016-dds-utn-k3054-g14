package ar.edu.utn.d2s.libraries.days;

import java.util.ArrayList;
import java.util.List;

public class DaysFactory{

	public List<Integer> fromMondayToFriday() {
		List<Integer> days = new ArrayList<Integer>();
		days.add(Days.getMonday());
		days.add(Days.getTuesday());
		days.add(Days.getWednesday());
		days.add(Days.getThursday());
		days.add(Days.getFriday());
		
		return days;
	}

	public List<Integer> fromMondayToSaturday() {
		List<Integer> days = this.fromMondayToFriday();
		days.add(Days.getSaturday());
		
		return days;
	}

}
