package ar.edu.utn.d2s.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import ar.edu.utn.d2s.interfaces.ExternalSource;
import ar.edu.utn.d2s.libraries.Schedule;
import ar.edu.utn.d2s.libraries.TimeInterval;
import ar.edu.utn.d2s.libraries.days.Days;
import ar.edu.utn.d2s.model.Direction;
import ar.edu.utn.d2s.model.Point;
import ar.edu.utn.d2s.model.Service;
import ar.edu.utn.d2s.model.poi.Bank;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.providers.BankDataProvider;

public class BankAdapter implements ExternalSource {

	protected BankDataProvider provider;
	
	/** Constructor */
	
	public BankAdapter(BankDataProvider provider) {
		super();
		this.provider = provider;
	}
	
	/** Implementations */
	
	@Override
	public List<PointOfInterest> getPointsOfInterest() throws Exception {
		
		JsonArray jsonResults = this.provider.getData();		

		List<PointOfInterest> response = new ArrayList<PointOfInterest>();
				
		for (JsonElement jsonElement : jsonResults) {
			Bank bank = this.adapt(jsonElement); // we adapt the Json to Bank
			response.add(bank);
		}
		
		return response;
	}
	
	@Override
	public List<PointOfInterest> search(String... keywords) throws Exception {				
		if (keywords.length != 2) {
			return new ArrayList<PointOfInterest>();
		}
		
		List<PointOfInterest> points = this.getPointsOfInterest();
		
		String bankName = keywords[0];
		String serviceName = keywords[1];
		
		return points.stream().filter(point ->
			point.getName().equals(bankName) && 
			((Bank) point).getServices().stream().anyMatch(service -> service.getName().equals(serviceName))
		 )
		 .collect(Collectors.toList());
	}
	
	/** Private Methods */

	/**
	 * Build a Bank instance from the data that holds the Json element
	 * 
	 * @param jsonElement
	 * @return
	 * @throws Exception
	 */
	private Bank adapt(JsonElement jsonElement) throws Exception {
		JsonObject jsonObject = (JsonObject) jsonElement;
		
		// First, the Point with the coordinates
	 	Point point = new Point(
 			jsonObject.get("x").getAsDouble(), 
 			jsonObject.get("y").getAsDouble()
		);

	 	// Then, the list of Services
		List<Service> services = new ArrayList<Service>();
		
		for (JsonElement jsonService : jsonObject.get("servicios").getAsJsonArray()) {
			Service service = this.getServiceFrom(jsonService);
			services.add(service);
		}
		
		// Direction not provided, will remain empty
		Direction direction = this.getDirectionFromCoordinates(point);
		
		// Name of the Bank
		String name = jsonObject.get("banco").getAsString().intern();
		
		// Finally, we build up the Bank instance
		Bank bank = new Bank(name ,point, direction, services);
		bank.setManager(jsonObject.get("gerente").getAsString().intern());
		bank.setLocation(jsonObject.get("sucursal").getAsString().intern());
		
		return bank;
	}
	
	/**
	 * Build a fake Schedule to create the Service
	 * 
	 * @param jsonService
	 * @return
	 * @throws Exception
	 */
	private Service getServiceFrom(JsonElement jsonService) throws Exception {
		Schedule schedule = new Schedule(Days.fromMondayToFriday(), TimeInterval.list("9:00 to 18:00"));
		
		return new Service(jsonService.getAsString().intern(), schedule);
	}

	/**
	 * Create a fake Direction "using" coordinates
	 * TODO: it would be optimal to have a Dictionary with points and directions
	 * 
	 * @param point
	 * @return
	 * @throws Exception
	 */
	private Direction getDirectionFromCoordinates(Point point) throws Exception {	
		return new Direction("Some street name", 1234);
	}
}
