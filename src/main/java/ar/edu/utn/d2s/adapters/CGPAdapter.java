package ar.edu.utn.d2s.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ar.edu.utn.d2s.dtos.DTOCgp;
import ar.edu.utn.d2s.dtos.DTOService;
import ar.edu.utn.d2s.dtos.DTOServiceRange;
import ar.edu.utn.d2s.interfaces.ExternalSource;
import ar.edu.utn.d2s.libraries.Schedule;
import ar.edu.utn.d2s.libraries.TimeInterval;
import ar.edu.utn.d2s.model.Direction;
import ar.edu.utn.d2s.model.Point;
import ar.edu.utn.d2s.model.Service;
import ar.edu.utn.d2s.model.poi.CGP;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.providers.CGPDataProvider;

public class CGPAdapter implements ExternalSource {
	
	protected CGPDataProvider provider;
	
	/** Constructor */
	
	public CGPAdapter(CGPDataProvider provider) {
		super();
		this.provider = provider;
	}
	
	/** Implementations */

	@Override
	public List<PointOfInterest> getPointsOfInterest() throws Exception {
		
		List<DTOCgp> DTOCgps = this.provider.getData();		

		List<PointOfInterest> response = new ArrayList<PointOfInterest>();
		
		for (DTOCgp DTOCgp : DTOCgps) {
			
			 
			String name = this.buildNameFrom(DTOCgp.getIncludedZones());
			Point coordinates = this.buildCoordinatesFrom(DTOCgp.getIncludedZones());
			Direction direction = this.buildDirectionFrom(DTOCgp.getAddress(),DTOCgp.getIncludedZones());
			int commune = DTOCgp.getCommune();
			List<Point> points = this.buildPointsFromZones(DTOCgp.getIncludedZones());
			List<Service> services = this.buildServicesFrom(DTOCgp.getServices());
						
			response.add(new CGP(name, coordinates, direction, commune, points, services));					
		}
		
		return response;
	}
	
	@Override
	public List<PointOfInterest> search(String... keywords) throws Exception {
			
		 
		if (keywords.length != 1) {
			return new ArrayList<PointOfInterest>();
		}
		
		String keyword = keywords[0];
				
		List<PointOfInterest> points = this.getPointsOfInterest();
		
	 	List<PointOfInterest> response = new ArrayList<PointOfInterest>();
	
	 	response.addAll(points.stream()
					 .filter(point -> point.getDirection().getCallePrincipal().equals(keyword))
					 .collect(Collectors.toList()));
		
		response.addAll(points.stream()
				 .filter(point -> point.getDirection().getBarrio().equals(keyword))
				 .collect(Collectors.toList()));
		
		return response;
	
		
	}
	
	/** Private Methods */
	
	private List<Service> buildServicesFrom(List<DTOService> services) throws Exception {
		
		List<Service> response = new ArrayList<Service>();
		
		for (DTOService DTOService : services) {
			Service service = this.buildServiceFrom(DTOService);
			response.add(service);
		}
		
		return response;
	}

	private List<Point> buildPointsFromZones(String includedZones) {
		// TODO: logic to somehow set a zone?
		
		ArrayList<Point> points = new ArrayList<Point>();
		
		points.add(new Point(-34.599771, -58.382430));
		points.add(new Point(-34.595761, -58.387172));
		points.add(new Point(-34.600142, -58.391571));
		points.add(new Point(-34.603921, -58.386550));
		
		return points;
	}

	private String buildNameFrom(String commune) {
		// TODO: add logic to map zones to the CGP's name?
		return commune;
	}

	private Point buildCoordinatesFrom(String includedZones) {
		// TODO: set some logic around this?
		return new Point(-34.600123, -58.386828);
	}

	private Service buildServiceFrom(DTOService DTOService) throws Exception {
		
		String name = DTOService.getName();		
		Schedule schedule = this.buildScheduleFrom(DTOService.getRanges());
		
		return new Service(name, schedule);
	}

	private Schedule buildScheduleFrom(List<DTOServiceRange> ranges) {
		
		List<Integer> days = new ArrayList<Integer>();
		List<TimeInterval> timeIntervals = new ArrayList<TimeInterval>();
		
		for (DTOServiceRange range : ranges) {
			days.add(range.getDayOfTheWeek());
			timeIntervals.add(new TimeInterval(
				range.getInitHour(), range.getEndHour(), range.getInitMinute(), range.getEndMinute()
			));
		}
		
		return new Schedule(days, timeIntervals);
	}

	private Direction buildDirectionFrom(String address,String zones) throws Exception {
		 
		String[] directionAttributes = address.split(" ");
			 
		return new Direction(directionAttributes[0], Integer.parseInt(directionAttributes[1]),zones);
	}
}
