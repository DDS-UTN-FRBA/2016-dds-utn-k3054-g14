package ar.edu.utn.d2s.services.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.utn.d2s.dao.QueryDAO;
import ar.edu.utn.d2s.model.Query;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.services.QueryService;

@Service("queryService")
@Transactional(propagation = Propagation.REQUIRED)
public class QueryServiceImpl implements QueryService{

	@Autowired
	private QueryDAO dao;
	
	@Override
	public void save(Query query) {
		dao.save(query);
		
	}

	@Override
	public void delete(Query query) {
		dao.delete(query);
		
	}

	@Override
	public void update(Query query) {
		dao.update(query);
		
	}

	@Override
	public Query getById(int id) {
		
		return dao.get(id);
	}

	@Override
	public List<Query> findAll() {
		 
		return dao.findAll();
	}

	@Override
	public List<Query> findByProperty(String propertyName, Object value) {
		 
		return dao.findyByProperty(propertyName, value);
	}

	@Override
	public Set<PointOfInterest> getResults(int query_id) {
		 
		return dao.get(query_id).getResults();
	}

}
