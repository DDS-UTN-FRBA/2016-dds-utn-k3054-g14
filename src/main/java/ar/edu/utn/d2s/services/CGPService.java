package ar.edu.utn.d2s.services;

import java.util.List;

import ar.edu.utn.d2s.model.Service;
import ar.edu.utn.d2s.model.poi.CGP;

public interface CGPService {

	void save(CGP cgp);
	void delete(CGP cgp);
	void update(CGP cgp);
	CGP getById(int id);
	List<CGP> findAll();
	List<CGP> findByProperty(String propertyName, Object value);
	
	List<Service> getServices(int cgp_id);
}
