package ar.edu.utn.d2s.services;

import java.util.List;

import ar.edu.utn.d2s.model.BusStop;
import ar.edu.utn.d2s.model.poi.Bus;

public interface BusService {
	void save(Bus bus);
	void delete(Bus bus);
	void update(Bus bus);
	Bus getById(int id);
	List<Bus> findAll();
	List<Bus> findByProperty(String propertyName, Object value);
	List<BusStop> getAllBusStops(int id);
}
