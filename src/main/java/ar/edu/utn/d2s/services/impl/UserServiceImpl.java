package ar.edu.utn.d2s.services.impl;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.utn.d2s.dao.UserDAO;
import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.services.UserService;
import ar.edu.utn.d2s.useractions.Action;

@Service("userService")
@Transactional(propagation = Propagation.REQUIRED)
public class UserServiceImpl implements UserService {


	@Autowired
	private UserDAO dao;
	
	@Override
	public void save(User user) {
		dao.save(user);
		
	}

	@Override
	public void delete(User user) {
		dao.delete(user);		
	}

	@Override
	public void update(User user) {
		dao.update(user);
		
	}

	@Override
	public User getById(int id) {
		 
		return dao.get(id);
	}

	@Override
	public List<User> findAll() {
		
		return dao.findAll();
	}

	@Override
	public List<User> findByProperty(String propertyName, Object value) {
		 
		return dao.findyByProperty(propertyName, value);
	}
	

	@Override
	public HashSet<Action> getServices(int id) {
		 
		return  dao.get(id).getUserActions();
	}

}
