package ar.edu.utn.d2s.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.utn.d2s.dao.BusDAO;
import ar.edu.utn.d2s.model.BusStop;
import ar.edu.utn.d2s.model.poi.Bus;
import ar.edu.utn.d2s.services.BusService;

@Service("busService")
@Transactional(propagation = Propagation.REQUIRED)
public class BusServiceImpl implements BusService {

	@Autowired
	private BusDAO dao;
	
	@Override
	public void save(Bus bus) {
		dao.save(bus);
		
	}

	@Override
	public void delete(Bus bus) {
		dao.delete(bus);
		
	}

	@Override
	public void update(Bus bus) {
		dao.update(bus);
		
	}

	@Override
	public Bus getById(int id) {
		 
		return dao.get(id);
	}

	@Override
	public List<Bus> findAll() {
		 
		return dao.findAll();
	}

	@Override
	public List<Bus> findByProperty(String propertyName, Object value) {
		 
		return dao.findyByProperty(propertyName, value);
	}

	@Override
	public List<BusStop> getAllBusStops(int id) {
		 
		return dao.get(id).getStops();
	}
	

}
