package ar.edu.utn.d2s.services;

import java.util.List;
import java.util.Set;

import ar.edu.utn.d2s.model.Query;
import ar.edu.utn.d2s.model.poi.PointOfInterest;

public interface QueryService {

	void save(Query query);
	void delete(Query query);
	void update(Query query);
	Query getById(int id);
	List<Query> findAll();
	List<Query> findByProperty(String propertyName, Object value);
	
	Set<PointOfInterest> getResults(int query_id);

}
