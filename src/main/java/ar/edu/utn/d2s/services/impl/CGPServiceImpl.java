package ar.edu.utn.d2s.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.utn.d2s.dao.CgpDAO;
import ar.edu.utn.d2s.model.poi.CGP;
import ar.edu.utn.d2s.services.CGPService;

@Service("cgpService")
@Transactional(propagation = Propagation.REQUIRED)
public class CGPServiceImpl implements CGPService{
	
	@Autowired
	private CgpDAO dao;

	@Override
	public void save(CGP cgp) {
		dao.save(cgp);
		
	}

	@Override
	public void delete(CGP cgp) {
		dao.delete(cgp);
		
	}

	@Override
	public void update(CGP cgp) {
		dao.update(cgp);
		
	}

	@Override
	public CGP getById(int id) {
		 
		return dao.get(id);
	}

	@Override
	public List<CGP> findAll() {
		 
		return dao.findAll();
	}

	@Override
	public List<CGP> findByProperty(String propertyName, Object value) {
		
		return dao.findyByProperty(propertyName, value);
	}

	@Override
	public List<ar.edu.utn.d2s.model.Service> getServices(int cgp_id) {
		 
		return dao.get(cgp_id).getServices();
	}

}
