package ar.edu.utn.d2s.services;

import java.util.List;

import ar.edu.utn.d2s.model.Service;
import ar.edu.utn.d2s.model.poi.Bank;
 

public interface BankService {

	void save(Bank bank);
	void delete(Bank bank);
	void update(Bank bank);
	Bank getById(int id);
	List<Bank> findAll();
	List<Bank> findByProperty(String propertyName, Object value);
 
	List<Service> getServices(int bank_id);
	String getManagerName(int id);
}
