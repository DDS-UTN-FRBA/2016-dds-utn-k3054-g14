package ar.edu.utn.d2s.services;

import java.util.HashSet;
import java.util.List;

import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.useractions.Action;
 

public interface UserService {

	void save(User user);
	void delete(User user);
	void update(User user);
	User getById(int id);
	List<User> findAll();
	List<User> findByProperty(String propertyName, Object value);
	HashSet<Action> getServices(int id);
	
}
