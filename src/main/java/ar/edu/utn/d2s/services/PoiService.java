package ar.edu.utn.d2s.services;

import java.util.List;

import ar.edu.utn.d2s.model.poi.PointOfInterest;


public interface PoiService {
	
	void save(PointOfInterest poi);
	void delete(PointOfInterest bank);
	void update(PointOfInterest bank);
	PointOfInterest getById(int id);
	List<PointOfInterest> findAll();
	List<PointOfInterest> findByProperty(String propertyName, Object value); 
}
