package ar.edu.utn.d2s.services;

import java.util.List;

import ar.edu.utn.d2s.libraries.Keyword;
import ar.edu.utn.d2s.model.poi.Shop;

public interface ShopService {
	void save(Shop shop);
	void delete(Shop shop);
	void update(Shop shop);
	Shop getById(int id);
	List<Shop> findAll();
	List<Shop> findByProperty(String propertyName, Object value);
	
	List<Keyword> getKeywords(int shop_id);

}
