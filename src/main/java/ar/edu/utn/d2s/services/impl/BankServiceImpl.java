package ar.edu.utn.d2s.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.utn.d2s.dao.BankDAO;
import ar.edu.utn.d2s.model.poi.Bank;
import ar.edu.utn.d2s.services.BankService;


@Service("bankService")
@Transactional(propagation = Propagation.REQUIRED)
public class BankServiceImpl implements BankService {


	@Autowired
	
	private BankDAO dao;
	
	@Override
	public void save(Bank bank) {
		dao.save(bank);		
	}

	@Override
	public void delete(Bank bank) {
		dao.delete(bank);		
	}

	@Override
	public void update(Bank bank) {
		dao.update(bank);		
	}

	@Override
	public List<Bank> findAll() {		
		return dao.findAll();
	}

	@Override
	public List<Bank> findByProperty(String propertyName, Object value) {		 
		return dao.findyByProperty(propertyName, value);
	}

	@Override
	public Bank getById(int id) {		
		return dao.get(id);
	}
	
	@Override
	public String getManagerName(int id) {
		 
		return dao.get(id).getManager();
	}

	@Override
	public List<ar.edu.utn.d2s.model.Service> getServices(int id) {
		 
		return dao.get(id).getServices();
	}
	
	
}
