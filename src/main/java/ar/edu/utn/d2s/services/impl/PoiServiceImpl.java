package ar.edu.utn.d2s.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.utn.d2s.dao.PoiDAO;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.services.PoiService;


@Service("poiService")
@Transactional(propagation = Propagation.REQUIRED)
public class PoiServiceImpl implements PoiService{

	@Autowired
	private PoiDAO dao;
	
	@Override
	public void save(PointOfInterest poi) {
		dao.save(poi);
	}

	@Override
	public void delete(PointOfInterest poi) {
		dao.delete(poi);		
	}

	@Override
	public void update(PointOfInterest poi) {
		dao.update(poi);
			}

	@Override
	public PointOfInterest getById(int id) {
		 return dao.get(id);
	}

	@Override
	public List<PointOfInterest> findAll() {		 
		return dao.findAll();
	}

	@Override
	public List<PointOfInterest> findByProperty(String propertyName,
			Object value) {		 
		return dao.findyByProperty(propertyName, value);
	}
	
	 
}
