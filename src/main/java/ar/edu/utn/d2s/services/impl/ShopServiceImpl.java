package ar.edu.utn.d2s.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.utn.d2s.dao.ShopDAO;
import ar.edu.utn.d2s.libraries.Keyword;
import ar.edu.utn.d2s.model.poi.Shop;
import ar.edu.utn.d2s.services.ShopService;

@Service("shopService")
@Transactional(propagation = Propagation.REQUIRED)
public class ShopServiceImpl implements ShopService {

	@Autowired
	private ShopDAO dao;
	
	@Override
	public void save(Shop shop) {
		dao.save(shop);
		
	}

	@Override
	public void delete(Shop shop) {
		dao.delete(shop);
		
	}

	@Override
	public void update(Shop shop) {
		dao.update(shop);
		
	}

	@Override
	public Shop getById(int id) {
		 
		return dao.get(id);
	}

	@Override
	public List<Shop> findAll() {
		 
		return dao.findAll();
	}

	@Override
	public List<Shop> findByProperty(String propertyName, Object value) {
		 
		return dao.findyByProperty(propertyName, value);
	}

	@Override
	public List<Keyword> getKeywords(int shop_id) {
		 
		return dao.get(shop_id).getKeywords();
	}

	
}
