package ar.edu.utn.d2s.exceptions.bus;

public class BusStopNotFound extends Exception {

	private static final long serialVersionUID = 1L;
 
	 public BusStopNotFound(String message) {
		 super(message);
	}
}
