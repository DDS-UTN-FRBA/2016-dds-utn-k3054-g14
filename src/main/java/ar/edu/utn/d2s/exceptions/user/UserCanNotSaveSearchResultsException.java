package ar.edu.utn.d2s.exceptions.user;

public class UserCanNotSaveSearchResultsException extends Exception{

	
	private static final long serialVersionUID = 1L;
	
	public UserCanNotSaveSearchResultsException(String message) {
		super(message);
	}
}


