package ar.edu.utn.d2s.exceptions;

public class PointOfInterestRepeatedException extends Exception {

	private static final long serialVersionUID = 1L;

	public PointOfInterestRepeatedException() {
		super("The point of interest already exists.");
	}
}

