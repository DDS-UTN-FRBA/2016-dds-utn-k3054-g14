package ar.edu.utn.d2s.exceptions;

public class PointOfInterestFactoryException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public PointOfInterestFactoryException(String message) {
		super(message);
	}
}
