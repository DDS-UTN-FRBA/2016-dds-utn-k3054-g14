package ar.edu.utn.d2s.exceptions.user;

public class UserCanNotReceiveEmailNotifException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public UserCanNotReceiveEmailNotifException(String message){
		super(message);
	}
}
