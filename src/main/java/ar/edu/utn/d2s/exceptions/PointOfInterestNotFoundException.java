package ar.edu.utn.d2s.exceptions;

public class PointOfInterestNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public PointOfInterestNotFoundException() {
		super("Point of interest not found.");
	}
}