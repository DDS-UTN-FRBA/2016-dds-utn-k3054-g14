package ar.edu.utn.d2s.exceptions.user;

public class ActionFailedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ActionFailedException(String message){
		super(message);
	}
}
