package ar.edu.utn.d2s.exceptions.user;

public class UserNotFoundException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public UserNotFoundException() {
		super("User not found.");
	}
}
