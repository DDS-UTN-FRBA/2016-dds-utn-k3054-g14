package ar.edu.utn.d2s.springmvc.rest;

import org.hibernate.Session;

import spark.Request;
import spark.Response;

@FunctionalInterface
public interface RouteWithSession {

	Object handle(Request request, Response response, Session session) throws Exception;
	
}
