package ar.edu.utn.d2s.reports;


import java.util.Map;
import java.util.stream.Collectors;

import ar.edu.utn.d2s.model.Query;
import ar.edu.utn.d2s.model.User;
 

public class SearchesByDatePerTerminalReport {


	public static Map<String,Long> find(User terminal)
 	{
 	 
 	   Map<String,Long> results = terminal.getSearchesRepo().getAllContent().stream()
 			  .collect(Collectors.groupingBy(Query::getDate,Collectors.counting()));
 	   
 	//  System.out.println(results);
 	   return results;
 
 	}
		
}

