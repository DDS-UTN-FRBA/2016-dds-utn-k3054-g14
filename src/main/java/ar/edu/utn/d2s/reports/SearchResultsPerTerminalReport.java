package ar.edu.utn.d2s.reports;

 
import java.util.ArrayList; 
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import ar.edu.utn.d2s.model.Query;
import ar.edu.utn.d2s.model.User;

public class SearchResultsPerTerminalReport {


	public static Map<String,Integer> find(List<User> terminals)
 	{
		
		 
		 List<Query> querys = new ArrayList<Query>();
		
		  
	 	 for(User terminal: terminals){
	 	   
	 		 querys.addAll(terminal.getSearchesRepo().getAllContent());
	 	 }
 	 
	 	 //cantidad de búsquedas de cada terminal
	 	 @SuppressWarnings("unused")
		Map<String, Long> counting = querys.stream().collect(
	                Collectors.groupingBy(Query::getUserName, Collectors.counting()));

	 	// System.out.println("Searches made by each Terminal");
	 	// System.out.println(counting);
	        
	     //sumatoria de resultados de todas las busquedas de cada terminal
	 	 Map<String, Integer> sum = querys.stream().collect(
	 			 Collectors.groupingBy(Query::getUserName, Collectors.summingInt(Query::getQtyResults)));
	 	
	 	// System.out.println("Quantity of POIs found");
	 	// System.out.println(sum);

 	 return sum;
 	 
 	}
}
