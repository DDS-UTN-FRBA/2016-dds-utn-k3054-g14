package ar.edu.utn.d2s.processes;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ExecutionResult {

	private LocalDateTime execDate;
	private List<String> status;
	private List<Integer> code;
	
	//**constructors**//
	
	public ExecutionResult(LocalDateTime execDate, List<Integer> code, List<String> status) {
		super();
		this.execDate = execDate;
		this.status = status;
		this.code=code;
		
	}
	
	public ExecutionResult(LocalDateTime localDateTime ) {
		this.execDate=localDateTime;
		this.code = new ArrayList<Integer>();
		this.status = new ArrayList<String>();
	}
	
	//**getters & setters *//
	
	public LocalDateTime getExecDate() {
		return execDate;
	}
	public void setExecDate(LocalDateTime execDate) {
		this.execDate = execDate;
	}
	public List<String> getStatus() {
		return status;
	}
	public void setStatus(List<String> status) {
		this.status = status;
	}

	public List<Integer> getCode() {
		return code;
	}

	public void setCode(List<Integer> code) {
		this.code = code;
	}

	public void add(Integer i, String string) {
		status.add(string);
		code.add(i);
		
	}
	
	
	
}
