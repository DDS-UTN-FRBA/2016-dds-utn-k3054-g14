package ar.edu.utn.d2s.processes;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import ar.edu.utn.d2s.exceptions.user.UserNotFoundException;
import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.repositories.UsersRepository;
import ar.edu.utn.d2s.useractions.Action;
import ar.edu.utn.d2s.useractions.ActionsByUser;

public class AddUserActionsProcess extends AsyncProcess {
	
	private User user;
	private UsersRepository userRepo;
	private List<ActionsByUser> newActions;
	private Boolean isComplete;

	
	//**constructor**//
	
	public AddUserActionsProcess(String name, UsersRepository userRepo, List<ActionsByUser> newActions) {
		
		super(name);
		this.userRepo = userRepo;
		this.newActions = newActions;
		this.isComplete = true;
		
	}
	
	
	//**methods **//
	
	
	
	@Override
	public ExecutionResult execute() {
		
		this.userRepo.setLastStateOfUsers(this.userRepo.getAllContent());
		
		this.setComplete(false);
		
		//we save the last state of the repo before any changes
		
		ExecutionResult result = new ExecutionResult(LocalDateTime.now());
		
		for(ActionsByUser aPair : this.getNewActions()){
			
			User auxUser = aPair.getUser();
			Set<Action> auxActions = aPair.getNewActions();
			
			if(this.userRepo.contains(auxUser)){
				try {
					this.userRepo.updateActions(auxUser, auxActions);
					result.add(200,"OK. "+ aPair.getUser().getName() + " User actions updated.");
					this.setComplete(true);
				} catch (UserNotFoundException e) {
					result.add(404,"ERROR. "+ aPair.getUser().getName() + " User not found.");
					this.setComplete(false);
				}
			}
		}
		
		if(!this.isComplete)			 
			this.userRepo.revertState();
			
		return result;
		
			
	}
		

	//**getters & setters **//
	
	public List<ActionsByUser> getNewActions() {
		return newActions;
	}

	public void setNewActions(List<ActionsByUser> newActions) {
		this.newActions = newActions;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UsersRepository getUserRepo() {
		return userRepo;
	}

	public void setUserRepo(UsersRepository userRepo) {
		this.userRepo = userRepo;
	}
 
	public Boolean isComplete() {
		return isComplete;
	} 
	
	public void setComplete(Boolean isComplete) {
		this.isComplete = isComplete;
	}

	
}
