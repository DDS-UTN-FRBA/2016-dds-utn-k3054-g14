package ar.edu.utn.d2s.processes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ar.edu.utn.d2s.exceptions.PointOfInterestFactoryException;
 

public class MultipleExecution extends AsyncProcess {

	private List <AsyncProcess> processes;
	private List <ExecutionResult> results;
	

	public MultipleExecution(String name, List<AsyncProcess> processes) {
		super(name);
		this.processes = processes;
		this.results = new ArrayList<ExecutionResult>();
	}	
	
	//** methods //
	
	
	public List<ExecutionResult> executeAll() throws FileNotFoundException, IOException, PointOfInterestFactoryException {
		
		for (AsyncProcess process : this.processes){
			this.results.add(process.execute());
		}
			
		return this.results;
		
	}

	@Override
	public ExecutionResult execute() throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
		return null;
	}
	
	//** getters & setters */
	
	public List<AsyncProcess> getProcesses() {
		return processes;
	}

	public void setProcesses(List<AsyncProcess> processes) {
		this.processes = processes;
	}
	
	public void addProcesses(AsyncProcess process) {
		this.processes.add(process);
	}

	
}
