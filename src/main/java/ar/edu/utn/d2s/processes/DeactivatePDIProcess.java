package ar.edu.utn.d2s.processes;

import java.time.LocalDateTime; 
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import ar.edu.utn.d2s.exceptions.PointOfInterestFactoryException;
import ar.edu.utn.d2s.model.BajaPDI;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.repositories.InMemoryRepository;
 

public class DeactivatePDIProcess extends AsyncProcess {

	
	private InMemoryRepository repo;
	private CopyOnWriteArrayList<PointOfInterest> POIs; 
	private  List<BajaPDI> POIsListToDelete;
	
	//**constructor**//
	
	public DeactivatePDIProcess(String name, InMemoryRepository repo, List<BajaPDI> POIsList) {
		super(name);
		this.setPOIs(new CopyOnWriteArrayList<PointOfInterest>()); 
		this.setRepo(repo); 
		this.setPOIsListToDelete(POIsList);
	}


	//**methods **//
	
	
	@Override
	public ExecutionResult execute() {

		ExecutionResult result = new ExecutionResult(LocalDateTime.now());

		for (BajaPDI aBajaPDI : this.getPOIsListToDelete()) {

			POIs.addAll(this.getRepo().searchLocal(aBajaPDI.getKeyword()));
			
			if (!POIs.isEmpty()) {
				for (PointOfInterest aPOI : POIs) {

					try {
						
						aPOI.deactivate();
						aPOI.setDeactivationDate(aBajaPDI.getDeactivateDate());
						result.add(200, "OK. " + aPOI.getName()
								+ " is not active anymore.");
						this.POIs.clear();

					} catch (PointOfInterestFactoryException e) {

						result.add(500, "Internal server error.");
					}

				}

			} else {
				result.add(404, "Error. " + aBajaPDI.getKeyword()
						+ " not found.");
			}
		}

		return result;
	}
	
	//** getters & setters**//
	
	public InMemoryRepository getRepo() {
		return repo;
	}

	public void setRepo(InMemoryRepository repo) {
		this.repo = repo;
	}

	public CopyOnWriteArrayList<PointOfInterest> getPOIs() {
		return POIs;
	}

	public void setPOIs(CopyOnWriteArrayList<PointOfInterest> pOIs) {
		POIs = pOIs;
	}


	public List<BajaPDI> getPOIsListToDelete() {
		return POIsListToDelete;
	}


	public void setPOIsListToDelete(List<BajaPDI> pOIsListToDelete) {
		POIsListToDelete = pOIsListToDelete;
	}
 
}
