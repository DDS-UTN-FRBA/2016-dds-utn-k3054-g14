package ar.edu.utn.d2s.processes;

import java.io.FileNotFoundException;
import java.io.IOException;

import ar.edu.utn.d2s.exceptions.PointOfInterestFactoryException;
 
public abstract class AsyncProcess {
	
	private String name;

	public AsyncProcess(String name) {
		super();
		this.name = name;
	}
	
	public AsyncProcess() {
		
	}
			
	//** methods **//
	
	public abstract ExecutionResult execute() throws FileNotFoundException, IOException, PointOfInterestFactoryException;

	/** getters & setters */
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
