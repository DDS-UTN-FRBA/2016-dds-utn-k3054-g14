package ar.edu.utn.d2s.processes;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException; 
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import ar.edu.utn.d2s.exceptions.PointOfInterestFactoryException;
import ar.edu.utn.d2s.hibernate.HibernateConfiguration;
import ar.edu.utn.d2s.libraries.Keyword;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.model.poi.Shop;
import ar.edu.utn.d2s.repositories.InMemoryRepository;
import ar.edu.utn.d2s.services.PoiService;
 

public class UpdateShopsProcess extends AsyncProcess {

	private InMemoryRepository repo;
	private CopyOnWriteArrayList<PointOfInterest> shops;
	private String filePath;
	
	AbstractApplicationContext context = new AnnotationConfigApplicationContext(HibernateConfiguration.class);
	
	PoiService poiService= (PoiService) context.getBean("poiService");
	
	//**constructor**//
	
	public UpdateShopsProcess(String name, String filePath,  InMemoryRepository repo) {
		super(name);
		this.shops = new CopyOnWriteArrayList<PointOfInterest>();
		this.filePath = filePath;
		this.repo =  repo;
	}


	//**methods **//

	@Override
	public ExecutionResult execute() throws FileNotFoundException, IOException,
			PointOfInterestFactoryException {

		ExecutionResult result = new ExecutionResult(LocalDateTime.now());
		 
		FileReader fr = new FileReader(filePath);
		BufferedReader br = new BufferedReader(fr);
		List<Keyword> keywords = new ArrayList<Keyword>();
		String shopName = null;

		try {
			String linea = null;
			String[] tokens = null;

			while ((linea = br.readLine()) != null) {

				tokens = linea.split(";");
				shopName = tokens[0];
				String[] keywordsArrayAux = tokens[1].split("\\s");
				
				 
				
				for (int i = 0; i < keywordsArrayAux.length; i++) {
					 
						
						keywords.add(new Keyword(keywordsArrayAux[i]));
					 
				}

				this.shops.addAll(this.repo.getAllLocal());
				
				for (PointOfInterest aPOI :shops ) {
					if (aPOI.getClass().equals(Shop.class)) {

						if (aPOI.getName() != null
								&& aPOI.getName().equals(shopName)) {
 
						
							
							((Shop) aPOI).setKeywords(new ArrayList<Keyword>(keywords));

							keywords.clear();
							result.add(200,"OK. "+ aPOI.getName() + " Shop Updated.");
							break;
						} else {

							 
								Shop newShop = new Shop(
										((Shop) aPOI).getSchedule(),
										((Shop) aPOI).getCategory(), shopName,
										aPOI.getCoordinates(), aPOI.getDirection(),
										new ArrayList<Keyword>(keywords));
								repo.addLocal(newShop);
								result.add(201,"OK. "+ newShop.getName() + " Shop Created.");
								keywords.clear();
								break;
							 
							
						}
					}

					 
				}
			}

		} catch (IOException e) {
			result.add(500,"Error. this POI is not a SHOP");
		}

		br.close();
		return result;
	}
	
	
	//**getters & setters*//
	

	public List<PointOfInterest> getShops() {
		return shops;
	}
	public void setShops(List<PointOfInterest> shops) {
		this.shops = (CopyOnWriteArrayList<PointOfInterest>) shops;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public InMemoryRepository getRepo() {
		return repo;
	}
	public void setRepo(InMemoryRepository repo) {
		this.repo = repo;
	}
	

}
