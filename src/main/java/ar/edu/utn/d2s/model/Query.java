package ar.edu.utn.d2s.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import ar.edu.utn.d2s.model.poi.PointOfInterest;

@Entity 
public class Query {
	
	@Column(name = "date")
	@NotNull
 	private String date;
	@Column(name = "user")
	@NotNull
 	private int user_id;
	@Transient
	private User user;
	@Column(name = "keyword")
	@NotNull
 	private String keyword;
	
	@OneToMany (cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
 	private Set<PointOfInterest> results;
	@Column(name = "sizeResults")
	@NotNull
 	private Integer sizeResults;
	@Column(name = "seekTime")
	@NotNull
 	private Long seekTime;
 	 
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	@NotNull
	private int id;

 	public Query(){};
 	
 	public Query(String date, User user, String keyword,  Set<PointOfInterest> results,
			Long seekTime) {
		super();
		
		results = new HashSet<PointOfInterest>();

		this.date = date;
		this.user_id=user.getId();
		this.user = user;
		this.keyword = keyword;
		this.results = results;
		this.seekTime = seekTime;
		this.sizeResults=results.size();
		
	}
 	
	//*getters & setters */
 	
 	
	public String getDate(){
			return this.date;
	}	
 

	public void setDate(String localTime) {
		this.date = localTime;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
		this.user_id=user.getId();
	}
	public String getUserName() {
		return user.name;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Set<PointOfInterest> getResults() {
		return results;
	}
	public void setResults(Set<PointOfInterest> results) {
		this.results = results;
	}
	public Long getSeekTime() {
		return seekTime;
	}
	public void setSeekTime(Long seekTime) {
		this.seekTime = seekTime;
	}

	public Boolean is(Query point) {
		return this.equals(point);
	}

	public Integer getQtyResults() {
		return sizeResults;
	}

	public void setQtyResults(Integer sizeResults) {
		this.sizeResults = sizeResults;
	}

	public int getUser_id() {
		return user_id;
		
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public Integer getSizeResults() {
		return sizeResults;
	}

	public void setSizeResults(Integer sizeResults) {
		this.sizeResults = sizeResults;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false; 
		Query other = (Query) obj;
		if (id != other.id)
			return false;
		
		return true;
	}
	
}
