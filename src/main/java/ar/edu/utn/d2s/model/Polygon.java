package ar.edu.utn.d2s.model;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity; 
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.eclipse.xtext.xbase.lib.CollectionLiterals;



@Entity
public class Polygon {

	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@OneToMany (cascade = CascadeType.ALL)
	private List<Point> surface;

	public Polygon() {
		ArrayList<Point> _arrayList = new ArrayList<Point>();
		this.surface = _arrayList;
	}

	public boolean add(final Point point) {
		return this.surface.add(point);
	}

	/**
	 * Constructor que le pasa un conjunto de Points que definen el polígono
	 */
	public Polygon(final List<Point> points) {
		this.surface = points;
	}



	public List<Point> getSurface() {
		return surface;
	}

	public void setSurface(List<Point> surface) {
		this.surface = surface;
	}


	public static Polygon asPolygon(final Point point) {
		return new Polygon(((List<Point>) Collections.<Point>unmodifiableList(CollectionLiterals.<Point>newArrayList(point))));
	}

 

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean pointOnVertex(final Point point) {
		return this.surface.contains(point);
	}

	public boolean isInside(Point point) {
	 
			int N = surface.size();
			int j = N - 1;
			boolean oddNodes = false;
			double x = point.longitude();
			double y = point.latitude();

			for (int i = 0; i < N; i++) {
				double verticeIY = surface.get(i).latitude();
				double verticeIX = surface.get(i).longitude();
				double verticeJY = surface.get(j).latitude();
				double verticeJX = surface.get(j).longitude();
				if ((verticeIY < y && verticeJY >= y || verticeJY < y && verticeIY >= y) &&
					(verticeIX <= x || verticeJX <= x)) {
					if (verticeIX + (y - verticeIY) / (verticeJY - verticeIY) * (verticeJX - verticeIX) < x) {
						oddNodes = !oddNodes;
								
					}
				}
				j = i;
			}
			return oddNodes;
		}
	 
}

