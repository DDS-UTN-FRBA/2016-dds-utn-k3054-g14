package ar.edu.utn.d2s.model;

import javax.persistence.Column; 
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id; 
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

 

@Entity
public class Category {
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@NotNull
	@Column(name = "category_id")
	private int id;
	
	@Column(name = "name")
	@NotNull
	@Size(min = 3, max = 60, message = "A Category's name can't have more than 60 characters")
	@Pattern(regexp = "[a-zA-Z]*", message = "A Category's name must have only letters")
	protected String name;
	
	@Column(name = "proximity")
	@NotNull
	protected double proximity; // [Km]
	


	/** Constructors */
	
	public Category(String name, double proximity) {
		this.name = name;
		this.proximity = proximity;
	}
	
	public Category( ) {
		 
	}
	/** Getters & setters */
	
	

	public String getName() {
		return name;
	}
	
 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	} 

	public void setName(String name) {
		this.name = name;
	}
	
	public double getProximity() {
		return proximity;
	}
	
	public void setProximity(double proximity) {
		this.proximity = proximity;
	}
}
