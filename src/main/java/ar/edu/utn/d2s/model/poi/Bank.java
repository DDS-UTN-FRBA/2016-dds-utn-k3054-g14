package ar.edu.utn.d2s.model.poi;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.joda.time.LocalTime;

import ar.edu.utn.d2s.exceptions.PointOfInterestFactoryException;
import ar.edu.utn.d2s.libraries.Schedule;
import ar.edu.utn.d2s.model.Direction;
import ar.edu.utn.d2s.model.Point;
import ar.edu.utn.d2s.model.Service;

@Entity
@PrimaryKeyJoinColumn(name = "bank_id", referencedColumnName = "pointOfInterest_id")
public class Bank extends PointOfInterest {
	
	
	@JoinColumn(name="bank_id",referencedColumnName="pointOfInterest_id", insertable=false, updatable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	@NotNull
	private int id;
	
	@OneToOne(cascade = CascadeType.ALL)

	@JoinColumn(name = "schedule_id")
	protected Schedule schedule;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Fetch(value = FetchMode.SUBSELECT)
	protected List<Service> services;
	
	@Column(name = "manager")
	@NotNull
	protected String manager;
	
	@Column(name = "location")
	@NotNull
	protected String location;
	
	/** Constructors  */

	public Bank(
			String name, 
			Point coordinates, 
			Direction direction, 
			Schedule schedule,  
			List<Service> services,String location , String manager) throws PointOfInterestFactoryException 
	{
		this(name, coordinates, direction, schedule);
		this.setServices(services);
		this.setLocation(location);
		this.setManager(manager);
	}
	
	public Bank(
			String name,
			Point coordinates, 
			Direction direction, 
			Schedule schedule) throws PointOfInterestFactoryException 
	{
		super(name, coordinates, direction);
		this.setSchedule(schedule);
	}
	
	
	public Bank(
			String name, 
			Point coordinates, 
			Direction direction, 
			List<Service> services) throws PointOfInterestFactoryException 
	{
		super(name, coordinates, direction);
		this.setServices(services);
	}
	
 
	
	public Bank() {
		this.isActive=true;
	}
	
	/** Getters & setters */
	
	public void setServices(List<Service> services) {
		this.services = services;
	};
	
	public List<Service> getServices() {
		return services;
	}
	
	public void setSchedule(Schedule schedule) {
		 this.schedule = schedule;
	}
			
	public Schedule getHorarioDeAtencion() {
		return schedule;
	}
	
	public void setManager(String manager) {
		this.manager = manager;
	}

	public String getManager() {
		return manager;		
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocation() {
		return location;
	}
	
	/** Override of abstract methods */
	
	@Override
	public boolean isAvailable(int day, LocalTime time) {
		return this.getServices()
		.stream()
		.anyMatch(service -> service.getSchedule().contains(day, time));
	}
	
	@Override
	public boolean isAvailable(int day, LocalTime time, String serviceName) {
		for (Service service: this.getServices()) {
			if (service.getName() == serviceName) {
				return service.getSchedule().contains(day, time);
			}
		}
		// service not found, bank not available
		return false; 
	}	

	@Override
	public boolean matches(String text) {
	    return this.name.startsWith(text);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public boolean is(Bank pointOfInterest) {
		return this.equals(pointOfInterest);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bank other = (Bank) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (coordinates == null) {
			if (other.coordinates != null)
				return false;
		} else if (!coordinates.equals(other.coordinates))
			return false;
		if (id != other.getId())
			return false;
		if (direction == null) {
			if (other.direction != null)
				return false;
		} else if (!direction.equals(other.direction))
			return false;	
		if (services == null) {
			if (other.services != null)
				return false;
		} else if (!services.equals(other.services))
			return false;
		if (manager==null) {
			if (other.manager != null)
				return false;
		} else if (!manager.equals(other.manager))
			return false;
		if (location==null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		return true;
	}
	
//	public List<String> getServicesNames() {
//		List<String> servicesNames = new ArrayList<String>();
//		for (Service service: this.getServices()) {
//			servicesNames.add(service.getName());
//		}
//		return servicesNames;
//	}

}
