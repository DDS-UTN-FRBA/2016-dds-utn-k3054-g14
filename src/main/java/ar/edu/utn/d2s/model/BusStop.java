package ar.edu.utn.d2s.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;


@Entity
@DiscriminatorValue(value="BusStop")
public class BusStop {
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@NotNull
	@Column(name = "busStop_id")
	private int id;


	@NotNull
	@OneToOne(cascade = CascadeType.ALL)
	protected Point coordinates;

	/** Constructors */
	
	public BusStop(Point point)  {		
		this.setCoordinates(point);
	}
	public BusStop( )  {}
	
	
	/** Getters & setters */
	
	public Point getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Point point) {
		this.coordinates = point;	
	}

	/**
	 * Build and return a list of bus stops by a list of points
	 * 
	 * @param points
	 * @return
	 */
	public static List<BusStop> list(Point... points) {
		List<BusStop> list = new ArrayList<BusStop>();
		
		for (Point point: points) {
			list.add(new BusStop(point));
		}
		
		return list;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false; 
		BusStop other = (BusStop) obj;
		if (this.coordinates != other.coordinates)
			return false;
		
		return true;
	}
}
