package ar.edu.utn.d2s.model.poi;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.joda.time.LocalTime;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;

import ar.edu.utn.d2s.exceptions.PointOfInterestFactoryException;
import ar.edu.utn.d2s.model.BusStop;
import ar.edu.utn.d2s.model.Direction;
import ar.edu.utn.d2s.model.Point;

@Entity
@PrimaryKeyJoinColumn(name = "bus_id", referencedColumnName = "pointOfInterest_id")
public class Bus extends PointOfInterest {
	
 
	@GeneratedValue(strategy=GenerationType.AUTO)
	@NotNull 
	@JoinColumn(name="bus_id",referencedColumnName="pointOfInterest_id", insertable=false, updatable=false)
	private int id;
	
	
	@Column(name = "line")
	@NotNull
    protected Integer line;
    
    @OneToMany( cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	protected List<BusStop> stops;
	
	/** Constructors */
	
	public Bus(
			String name, 
			Point coordinates, 
			Direction direction, 
			Integer line, 
			List<BusStop> stops) 
			throws PointOfInterestFactoryException
	{
		super(name, coordinates, direction);
		this.setLine(line);
		this.setStops(stops);
	}
	
	public Bus( ){
		this.isActive=true;
	}
	
	/** Getters & setters */

	public List<BusStop> getStops() {
		return stops;
	}
	
	public void setStops(List<BusStop> stops) throws PointOfInterestFactoryException {
		
		if (stops.equals(null)) {
			throw new PointOfInterestFactoryException("The list of stops can't be null.");
		}
		
		this.stops = stops;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) throws PointOfInterestFactoryException {
		if (line <= 0) {
			throw new PointOfInterestFactoryException("The bus line must be greater than zero.");
		}
			
		this.line = line;
	}
	
	/** Helper methods */
	
	void addStop(BusStop stop) {
		this.stops.add(stop);
	}
	
	/** Override abstract methods */

	@Override
	public boolean isAvailable(int day, LocalTime time) {
		return true;
	}
	
	@Override
	public boolean isAvailable(int day, LocalTime time, String serviceName) {
		return true;
	}	

	@Override
	public boolean matches(String text) {
		try {
			return Integer.parseInt(text) == this.line; 
		}
		catch (NumberFormatException e) {
			// couldn't cast the text to integer
			return false;
		}
		
	}
	
	
	public boolean isCloseTo(Point punto) {
		return this.getStops()
				.stream()
				.anyMatch(stop -> stop.getCoordinates().distance(punto) <= 0.1);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}	
	
	
	public boolean is(Bus pointOfInterest) {
		return this.equals(pointOfInterest);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bus other = (Bus) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (coordinates == null) {
			if (other.coordinates != null)
				return false;
		} else if (!coordinates.equals(other.coordinates))
			return false;
		if (id != other.getId())
			return false;
		if (direction == null) {
			if (other.direction != null)
				return false;
		} else if (!direction.equals(other.direction))
			return false;	
		if (line == null) {
			if (other.line != null)
				return false;
		} else if (!line.equals(other.line))
			return false;
	 
		 
		return true;
	}
	
}
