package ar.edu.utn.d2s.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.joda.time.LocalTime;

import ar.edu.utn.d2s.libraries.Schedule;


@Entity
@Table(name="Servicios")
public class Service {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "service_id")
	private int id;
	
	@Column(name = "name")
	@NotNull
	@Size(min = 3, max = 60, message = "A Category's name can't have more than 60 characters")
	@Pattern(regexp = "[a-zA-Z]*", message = "A Category's name must have only letters")
	protected String name;
	
	@OneToOne(cascade = CascadeType.ALL)
	protected Schedule schedule;
	
	/** Constructors */
	
	public Service(String name, Schedule schedule) throws Exception  {
		super();
		this.setName(name);
		this.setSchedule(schedule);	
	}
	
	public Service()  {
		super();		
	}
	
	/**
	 * Build and return a list of services
	 * 
	 * @param services
	 * @return
	 */
	public static List<Service> list(Service... services) {		
		return Arrays.stream(services).collect(Collectors.toList());
	}
	
	/**
	 * Determine if the day and time are contained in the service's schedule
	 * 
	 * @param day
	 * @param time
	 * @return
	 */
	public boolean containsInSchedule(int day, LocalTime time) {
		return this.getSchedule().contains(day, time);
	}
	
	/** Getters & setters */

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) throws Exception {
		if (name.isEmpty()) {
			throw new Exception("The name of the service can't be null.");
		}
		
		this.name = name;
	}
	
	public Schedule getSchedule() {
		return schedule;
	}
	
	public void setSchedule(Schedule schedule) throws Exception {
		if (schedule.equals(null)) {
			throw new Exception("The schedule can't be null.");
		}
		
		this.schedule = schedule;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false; 
		Service other = (Service) obj;
		if (id != other.id)
			return false;
		
		return true;
	}
}
