package ar.edu.utn.d2s.model.poi;

import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.joda.time.LocalTime;

import ar.edu.utn.d2s.exceptions.PointOfInterestFactoryException;
import ar.edu.utn.d2s.model.Direction;
import ar.edu.utn.d2s.model.Point;
import ar.edu.utn.d2s.model.Polygon;
import ar.edu.utn.d2s.model.Service;


@Entity
@PrimaryKeyJoinColumn(name = "cgp_id", referencedColumnName = "pointOfInterest_id")
public class CGP extends PointOfInterest {
	
 
	@JoinColumn(name="cgp_id",referencedColumnName="pointOfInterest_id", insertable=false, updatable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	@NotNull
	private int id;
	
	@Column(name = "commune")
	@NotNull
	protected Integer commune;	
	@OneToMany (cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	@NotNull
	@Fetch(value = FetchMode.SUBSELECT)
	protected List<Service> services;	
	@OneToOne(cascade = CascadeType.ALL)
	@NotNull
	@JoinColumn(name = "zone_id")
	protected Polygon zone;
	
	/** Constructors */

	public CGP(
			String name, 
			Point coordinates, 
			Direction direction, 
			Integer commune, 
			List<Point> points) 
			throws PointOfInterestFactoryException 
	{
		super(name, coordinates, direction);
		this.setCommune(commune);
		this.setZone(points);
	}
	
	public CGP(
			String name, 
			Point coordinates, 
			Direction direction, 
			Integer commune, 
			List<Point> points,
			List<Service> services
			) 
			throws PointOfInterestFactoryException 
	{
		this(name, coordinates, direction, commune, points);
		this.setServices(services);
	}

	public CGP( ) {
		this.isActive=true;
	}
		 
	/** Getters & Setters */
	
	public Polygon getZone() {
		return zone;
	}
	
	public void setZone(List<Point> points) throws PointOfInterestFactoryException {
		
		if (points.equals(null) || points.size() == 0) {
			throw new PointOfInterestFactoryException("List of POIs can't be empty.");
		}
			
		this.zone = new Polygon(points);
	}
	
	public int getCommune() {
		return commune;
	}

	public void setCommune(int commune) throws PointOfInterestFactoryException {
		if (commune <= 0) {
			throw new PointOfInterestFactoryException("The commune number must be greater than zero");
		}
			
		this.commune = commune;
	}

	public List<Service> getServices() {
		return services;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	};

	/** Helper methods */
	
	public void addPoint(Point punto) {
		this.zone.add(punto);
	}

	public void addService(Service service) throws Exception {
		if (service.equals(null)) {
			throw new Exception("The service can't be null.");
		}
		
		this.services.add(service);
	} 
	
	
	/** Override abstract methods */
	
	@Override
	public boolean isAvailable(int day, LocalTime time) {
		return this.getServices()
			.stream()
			.anyMatch(service -> service.containsInSchedule(day, time));
	}
	
	@Override
	public boolean isAvailable(int day, LocalTime time, String serviceName) {
		Optional<Service> foundService = this.getServices()
			.stream()
			.filter(service -> service.getName().equals(serviceName))
			.findFirst();
		
		if (foundService.isPresent()) {
			return foundService.get().containsInSchedule(day, time);
		}
		
		// service not found, not available
		return false; 
	}	

	@Override
    public boolean matches(String text) {
		try {
			if (Integer.parseInt(text) == this.getCommune()) {
				return true;
			}
		}
		catch (NumberFormatException e) {
			// couldn't cast the text parameter to integer
			// that means it could be the name of a service
		}
		
		return this.getServices()
				.stream()
				.anyMatch(service -> service.getName().startsWith(text));    
	}

	@Override
	public boolean isCloseTo(Point point) {
		return super.isCloseTo(point) &&
			   this.zone.isInside(point) && 
			   this.zone.pointOnVertex(point);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setZone(Polygon zone) {
		this.zone = zone;
	}
	
	public boolean is(CGP pointOfInterest) {
		return this.equals(pointOfInterest);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CGP other = (CGP) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
	 
		if (id != other.id)
			return false;
		if (direction == null) {
			if (other.direction != null)
				return false;
		} else if (!direction.equals(other.direction))
			return false;	
		
		if (commune==null) {
			if (other.commune != null)
				return false;
		} else if (!commune.equals(other.commune))
			return false;
		 
		return true;
	}
	
}
