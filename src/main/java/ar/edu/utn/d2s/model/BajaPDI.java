package ar.edu.utn.d2s.model;

import java.time.LocalDate;

public class BajaPDI {

	private String keyword;
	private LocalDate deactivateDate;
	
	
	//**constructor**//
	
	public BajaPDI(String keyword, LocalDate deactivateDate) {
		super();
		this.keyword = keyword;
		this.deactivateDate = deactivateDate;
	}
	
	 

	/**getters & setters **/
	
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public LocalDate getDeactivateDate() {
		return deactivateDate;
	}
	public void setDeactivateDate(LocalDate deactivateDate) {
		this.deactivateDate = deactivateDate;
	}
	
	
}
