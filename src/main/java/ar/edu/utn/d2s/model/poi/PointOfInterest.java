package ar.edu.utn.d2s.model.poi;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.joda.time.LocalTime;

import ar.edu.utn.d2s.exceptions.PointOfInterestFactoryException;
import ar.edu.utn.d2s.model.Direction;
import ar.edu.utn.d2s.model.Point;


@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@Table(name="pointOfInterest")
public abstract class PointOfInterest {

	@Column(name = "pointOfInterest_id")
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@NotNull
	private int id;
	
	@Column(name = "name")
	@NotNull
	@Size(min = 3, max = 60, message = "A POI's name can't have more than 60 characters")
	@Pattern(regexp = "[a-zA-Z]*", message = "A POI's name must have only letters")
	protected String name;	
	
	@NotNull
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "pointOfInterest_id")
 	protected Point coordinates;
	
	@NotNull
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "pointOfInterest_id")
	protected Direction direction;
	@Column(name = "isActive")
	@NotNull
	protected Boolean isActive;
	@Column(name = "deactivationDate") 
	protected LocalDate deactivationDate;
	/** Constructors */
	
	public PointOfInterest() {
		// explicit empty constructor
		this.isActive=true;
	}
	
	public PointOfInterest(String name, Point coordinates, Direction direction) throws PointOfInterestFactoryException {
		this.setName(name);
		this.setCoordinates(coordinates);
		this.setDirection(direction); 
		this.isActive=true;
	}
	
	
	
	/** abstract methods  */
	
	public abstract boolean isAvailable(int day, LocalTime time);
	public abstract boolean isAvailable(int day, LocalTime time, String service);
	public abstract boolean matches(String text);
	public boolean isCloseTo(Point point) {
		return this.coordinates.distance(point) < 0.5;
	}
	
	/** helper methods */
	
	/**
	 * Two POIs are equals if they are the exact same object
	 * 
	 * @param pointOfInterest
	 * @return
	 */
	
	public Boolean isActive(){
		return this.isActive;
	}
	
	public boolean is(PointOfInterest pointOfInterest) {
		return this.equals(pointOfInterest);
	}
	
	public void deactivate() throws PointOfInterestFactoryException{
		if(this.isActive){
		this.isActive=false;
		 
		}
		else
			throw new PointOfInterestFactoryException("The POI is already deactivated.");
	}
	
	public void activate() throws PointOfInterestFactoryException{
		if(!this.isActive){
			this.isActive=true;
		 
			}
			else
				throw new PointOfInterestFactoryException("The POI is already active.");
	}

/** Getters & setters */
	
	public String getName() {
		return name;
	}

	public void setName(String name) throws PointOfInterestFactoryException {
		if (name == null || name.isEmpty()) {
			throw new PointOfInterestFactoryException("A POI must have a name.");
		}
		
		if (name.length() > 60) {
			throw new PointOfInterestFactoryException("A POI's name can't have more than 60 characters");
		}
		
		this.name = name;
	}

	public Point getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Point coordinates) throws PointOfInterestFactoryException {
		
		if (coordinates == null) {
			throw new PointOfInterestFactoryException("A POI must have coordinates.");			 
		}
		
		this.coordinates = coordinates;
	}	
	
	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) throws PointOfInterestFactoryException {
		
		if (direction == null) {
			throw new PointOfInterestFactoryException("A POI must have a direction.");
		}
		
		this.direction = direction;
	}	
	
	public LocalDate getDeactivationDate() {
		return deactivationDate;
	}

	public void setDeactivationDate(LocalDate deactivationDate) {
		this.deactivationDate = deactivationDate;
	}
	 
	public int getId()
	{
		return id;
	}
	
	public void setId(int ID)
	{
		this.id = ID;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false; 
		PointOfInterest other = (PointOfInterest) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (coordinates == null) {
			if (other.coordinates != null)
				return false;
		} else if (!coordinates.equals(other.coordinates))
			return false;
		if (id != other.id)
			return false;
		if (direction == null) {
			if (other.direction != null)
				return false;
		} else if (!direction.equals(other.direction))
			return false;
		return true;
	}

}
