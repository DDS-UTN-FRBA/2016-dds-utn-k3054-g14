package ar.edu.utn.d2s.model.poi;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;  
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.joda.time.LocalTime;

import ar.edu.utn.d2s.exceptions.PointOfInterestFactoryException;
import ar.edu.utn.d2s.libraries.Keyword;
import ar.edu.utn.d2s.libraries.Schedule;
import ar.edu.utn.d2s.model.Category;
import ar.edu.utn.d2s.model.Direction;
import ar.edu.utn.d2s.model.Point;

@Entity
@PrimaryKeyJoinColumn(name = "shop_id", referencedColumnName = "pointOfInterest_id")
public class Shop extends PointOfInterest {
	
 
	@JoinColumn(name="shop_id",referencedColumnName="pointOfInterest_id", insertable=false, updatable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	@NotNull
	private int id;
		
	@NotNull
	@OneToOne(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	protected Schedule schedule; 
	
	@OneToMany (cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@NotNull
	protected List<Keyword> keywords;
	
	@NotNull
	@OneToOne(cascade = CascadeType.ALL,fetch=FetchType.EAGER) 
	protected Category category;

	/** Constructors */
	
	public Shop(Schedule schedule,
			Category category,
			String name, 
			Point coordinates, 
			Direction direction,
			List<Keyword> keywords) throws PointOfInterestFactoryException {
		
		super(name, coordinates, direction);
		
		this.setSchedule(schedule);
		this.setName(name);
		this.setKeywords(keywords);
		this.setCategory(category);
	}

	public Shop() {
		super();
		this.keywords = new ArrayList<Keyword>();
		this.isActive=true;
	}

	/** Getters & Setter */
	
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	 
	public String getName() {
		return name;
	}

	public void setName(String name) throws PointOfInterestFactoryException {
		if (name.length() == 0) {
			throw new PointOfInterestFactoryException("The name of the shop can't be null.");
		}
		
		this.name = name;
	}

	public List<Keyword> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<Keyword> keywords) throws PointOfInterestFactoryException {
		if (keywords.equals(null) || keywords.size() == 0) {
			throw new PointOfInterestFactoryException("The keywords can't be null or empty.");
		}

		this.keywords = keywords;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule setSchedule) {
		this.schedule = setSchedule;
	}	
	
	/** Override abstract methods */
	
	@Override
	public boolean isAvailable(int day, LocalTime time) {
		return this.getSchedule().contains(day, time);
	}
	
	@Override
	public boolean isAvailable(int dia, LocalTime hora, String servicio) {
		return this.isAvailable(dia, hora);
	}	

	@Override
	public boolean matches(String text) {
		if (this.name.toLowerCase().startsWith(text.toLowerCase())) {
			return true;
		}
	    
		if (this.keywords.stream().anyMatch(keyword -> keyword.getWord().toLowerCase().contains(text.toLowerCase()))) {
			return true;
		}  
	    
	    if (this.category.getName().toLowerCase().contains(text.toLowerCase())) {
	    	return true;
	    }        
	      
	    
	    return false;
	}

	@Override
	public boolean isCloseTo(Point point) {
		return this.coordinates.distance(point) < this.category.getProximity();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public boolean is(Shop pointOfInterest) {
		return this.equals(pointOfInterest);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Shop other = (Shop) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (coordinates == null) {
			if (other.coordinates != null)
				return false;
		} else if (!coordinates.equals(other.coordinates))
			return false;
		if (id != other.id)
			return false;
		if (direction == null) {
			if (other.direction != null)
				return false;
		} else if (!direction.equals(other.direction))
			return false;
		if (keywords == null) {
			if (other.keywords != null)
				return false;
		} else if (!keywords.equals(other.keywords))
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		return true;
	}

}

 
 
