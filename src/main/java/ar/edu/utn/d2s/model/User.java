package ar.edu.utn.d2s.model;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.joda.time.DateTime;

import ar.edu.utn.d2s.exceptions.user.ActionFailedException;
import ar.edu.utn.d2s.exceptions.user.UserCanNotReceiveEmailNotifException;
import ar.edu.utn.d2s.exceptions.user.UserCanNotSaveSearchResultsException;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.repositories.SearchResultsRepository;
import ar.edu.utn.d2s.useractions.Action;
 

@Entity 
public class User {
	 
		 
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	@NotNull
	private int id;
	
	@Column(name = "saveSearches")
	@NotNull
	Boolean saveSearches;
	
	@Column(name = "name")
	@NotNull
	@Size(min = 3, max = 60, message = "A User's name can't have more than 60 characters")
	@Pattern(regexp = "[a-zA-Z]*", message = "A User's name must have only letters")
	String name;  

	@Column(name = "isAdmin")
	@NotNull
	Boolean isAdmin;
	
	 
	@OneToMany (cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	Set<Action> userActions;
	@Transient
	SearchResultsRepository searchesRepo;
	 
	
	
	public User(){
		this.userActions= new HashSet<Action>();
		this.searchesRepo = new SearchResultsRepository(); 
		this.saveSearches = true;
		this.isAdmin=false;
	}
	
	public User(String name) {
		super();
		this.userActions= new HashSet<Action>();
	
		this.searchesRepo = new SearchResultsRepository();
		this.saveSearches = true;
		this.name = name;
		this.isAdmin=false;
		
	}
	
	public User(Boolean saveSearches, String name, Boolean isadmin) {
		super();
		this.isAdmin=isadmin;
		this.userActions= new HashSet<Action>();
		this.saveSearches = saveSearches;
		this.name = name;
	}
	
	
	
	//**actions**//
	
	public void executeActions(String keyword, List<PointOfInterest> pdis, DateTime initDateTime, DateTime endDateTime) throws UserCanNotReceiveEmailNotifException, ActionFailedException {
		for (Action actionToExec : this.getUserActions()) {
			
			if(actionToExec.isActive())
				actionToExec.action(keyword, pdis, initDateTime,endDateTime, this);
			else
				throw new ActionFailedException("Action is not activated");
		}
	
	}
	
	
	public void emailNotification() throws UserCanNotReceiveEmailNotifException {
		 if(!this.isAdmin()){
			 throw new UserCanNotReceiveEmailNotifException("This user is not a System Administrator");
	 }
	 else
	 {
		 //recieve email
		 }
		
	} 
	
	public boolean isAdmin() { 
		return this.isAdmin;
	}
	
	public void saveSearchResults(Boolean decision) throws UserCanNotSaveSearchResultsException{
		if(!this.isAdmin())
			this.saveSearches=decision;		
		else
			throw new UserCanNotSaveSearchResultsException("This user is not Terminal");
		
	}
	
	
	//** getters ands setters **/ 	
	
	
	
	public Boolean getSaveSearches() {
		return saveSearches;
	}
	
	public void addUserActions(Action newAction) {
		this.userActions.add(newAction);
	}
	
	public HashSet<Action> getUserActions() {
		return (HashSet<Action>) userActions;
	}
	
	public void setUserActions(HashSet<Action> userActions) {
		this.userActions = userActions;
	}
	
	public void setSaveSearches(Boolean saveSearches) {
		this.saveSearches = saveSearches;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public SearchResultsRepository getSearchesRepo() {
		return searchesRepo;
	}
	
	public void setSearchesRepo(SearchResultsRepository searchesRepo) {
		this.searchesRepo = searchesRepo;
	}
	
	public void setUserActions(Set<Action> userActions) {
		this.userActions = userActions;
	}
	
	public Boolean is(User user) {
		return this.equals(user);
	}

	public int getId() {
		return this.id;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	
}
