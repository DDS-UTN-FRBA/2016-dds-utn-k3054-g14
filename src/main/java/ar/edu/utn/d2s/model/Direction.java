package ar.edu.utn.d2s.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern; 
 

@Entity
public class Direction {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "direction_id")
	private int id;
	
	
	@Column(name = "callePrincipal")
	@NotNull	 
	protected String callePrincipal;
	@Column(name = "entreCalle1")
	@NotNull
	protected String entreCalle1;
	@Column(name = "entreCalle2")
	@NotNull
	protected String entreCalle2;
	@Column(name = "numero")
	@Min(value= 0, message = "La altura de la calle no puede ser negativa.")
	@NotNull 
	protected int numero;
	@Column(name = "piso")
	@NotNull
	@Min(value= 0, message = "El número de Piso no puede ser negativo.")
	protected int piso;
	@Column(name = "departamento")
	@NotNull
	@Pattern(regexp = "[a-zA-Z]*", message = "El número de departamento debe ser una letra.")
	protected char departamento;
	@Column(name = "unidad")
	@NotNull
	@Min(value= 0, message = "El número de unidad no puede ser negativo.")
	protected int unidad;
	@Column(name = "codigoPostal")
	@NotNull
	@Min(value= 0, message = "El número de codigo Postal no puede ser negativo.")
	protected int codigoPostal;
	@Column(name = "localidad")
	@NotNull
	protected String localidad;
	@Column(name = "barrio")
	@NotNull
	protected String barrio;
	@Column(name = "provincia")
	@NotNull
	protected String provincia;
	@Column(name = "pais")
	@NotNull
	protected String pais;
	
	/** Constructors */
	
	public Direction(String callePrincipal, 
			String entreCalle1,
			String entreCalle2, 
			int numero, int piso, 
			char departamento,
			int unidad, 
			int codigoPostal, 
			String localidad, 
			String barrio,
			String provincia, 
			String pais) throws Exception  {
		
		super();
				
		this.setCallePrincipal(callePrincipal);
		this.setEntreCalle1(entreCalle1);
		this.setEntreCalle2(entreCalle2);
		this.setNumero(numero);
		this.setPiso(piso);
		this.setDepartamento(departamento);
		this.setUnidad(unidad);
		this.setCodigoPostal(codigoPostal);
		this.setLocalidad(localidad);
		this.setBarrio(barrio);
		this.setProvincia(provincia);
		this.setPais(pais);
	}
	
	public Direction(String callePrincipal, int numero, String zonasIncluidas) throws Exception {
		super();
		this.setNumero(numero);
		this.setCallePrincipal(callePrincipal);
		this.setBarrio(zonasIncluidas);
	}
	
	public Direction(String callePrincipal, int numero ) throws Exception {
		super();
		this.setNumero(numero);
		this.setCallePrincipal(callePrincipal); 
	}
	
	public Direction(  )  {
		super();
		 
	}


	/** Getters & setters */
	
	public String getCallePrincipal() {
		return callePrincipal;
	}
	
	public void setCallePrincipal(String callePrincipal) throws Exception {
		if (callePrincipal.length() <= 0) {
			throw new Exception("El nombre de la Calle Principal no puede ser nulo.");
		}
		
		this.callePrincipal = callePrincipal;		
	}
	
	public String getEntreCalle1() {
		return entreCalle1;
	}
	
	public void setEntreCalle1(String entreCalle1)  throws Exception {
		if (entreCalle1.length() <= 0) {
			throw new Exception("El nombre de la Calle no puede ser nulo.");
		}
		
		this.entreCalle1 = entreCalle1;	
	}
	
	public String getEntreCalle2() {
		return entreCalle2;
	}
	
	public void setEntreCalle2(String entreCalle2)  throws Exception {
		if (entreCalle2.length() <= 0) {
			throw new Exception("El nombre de la Calle no puede ser nulo.");
		}
		
		this.entreCalle2 = entreCalle2;	
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) throws Exception {
		if (numero < 0) {
			throw new Exception("La altura de la Calle no puede ser negativa.");
		}

		this.numero = numero;	
	}
	
	public int getPiso() {
		return piso;
	}
	
	public void setPiso(int piso) throws Exception {
		if (piso < 0) {
			throw new Exception("El número de Piso no puede ser negativo.");
		}
		
		this.piso = piso;	
	}
	
	public char getDepartamento() {
		return departamento;
	}
	
	public void setDepartamento(char departamento) throws Exception {
		if (!Character.isLetter(departamento)) {
			throw new Exception("El número de departamento debe ser una letra.");
		}
		
		this.departamento = departamento;	
	}
	
	public int getUnidad() {
		return unidad;
	}
	
	public void setUnidad(int unidad) throws Exception {
		if (unidad < 0) {
			throw new Exception("El número de unidad no puede ser negativo.");
		}
		
		this.unidad = unidad;	
	}
	
	public int getCodigoPostal() {
		return codigoPostal;
	}
	
	public void setCodigoPostal(int codigoPostal) throws Exception {
		if (codigoPostal < 0) {
			throw new Exception("El número de código Postal no puede ser negativo.");
		}
		
		this.codigoPostal = codigoPostal;	
	}
	
	public String getLocalidad() {
		return localidad;
	}
	
	public void setLocalidad(String localidad) throws Exception {
		if (localidad.length() <= 0) {
			throw new Exception("El nombre de la localidad no puede ser nulo.");
		}
		
		this.localidad = localidad;
	}
	
	public String getBarrio() {
		return barrio;
	}
	
	public void setBarrio(String barrio) throws Exception {
		if (barrio.length() <= 0) {
			throw new Exception("El nombre del barrio no puede ser nulo.");
		}
		
		this.barrio = barrio;	
	}
	
	public String getProvincia() {
		return provincia;
	}
	
	public void setProvincia(String provincia) throws Exception {
		if (provincia.length() <= 0) {
			throw new Exception("El nombre de la provincia no puede ser nulo.");
		}
		
		this.provincia = provincia;	
	}
	
	public String getPais() {
		return pais;
	}
	
	public void setPais(String pais) throws Exception {
		if (pais.length() <= 0) {
			throw new Exception("El nombre del País no puede ser nulo.");
		}
		
		this.pais = pais;	
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false; 
		Direction other = (Direction) obj;
		if (id != other.id)
			return false;
		
		return true;
	}
}

