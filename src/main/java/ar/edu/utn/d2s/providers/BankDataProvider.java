package ar.edu.utn.d2s.providers;

import com.google.gson.JsonArray;

public interface BankDataProvider {

	/**
	 * Provide a JSON Array with all the banks
	 * 
	 * @return
	 */
	public JsonArray getData();
}
