package ar.edu.utn.d2s.providers;

import java.util.List;

import ar.edu.utn.d2s.dtos.DTOCgp;

public interface CGPDataProvider {

	/**
	 * Provide with a list of all the DTOCgps the provider contains
	 * 
	 * @return
	 */
	public List<DTOCgp> getData();
}
