package ar.edu.utn.d2s.providers;

import java.util.List;

import ar.edu.utn.d2s.model.BajaPDI;
 

public interface BajaPDIProvider {

	/**
	 * provide with a list of points of interest from an external source to eliminate
	 * 
	 * @return 
	 */
	public List<BajaPDI> getData();
}
