package ar.edu.utn.d2s.dtos;
 
import java.util.List;
 
public class DTOCgp {

	int commune;
	String zones;
	String director;
	String address;
	String phone;
	List<DTOService> services;
	
	public DTOCgp(int commune, String zones, String director, String address, String phone, List<DTOService> services) {
		super();
		this.commune = commune;
		this.zones = zones;
		this.director = director;
		this.address = address;
		this.phone = phone;
		this.services = services;
	}

	public int getCommune() {
		return commune;
	}

	public void setCommune(int commune) {
		this.commune = commune;
	}

	public String getIncludedZones() {
		return zones;
	}

	public void setIncludedZones(String includedZones) {
		this.zones = includedZones;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<DTOService> getServices() {
		return services;
	}

	public void setServices(List<DTOService> services) {
		this.services = services;
	}
}