package ar.edu.utn.d2s.dtos;

public class DTOServiceRange {
	
	int dayOfTheWeek;
	int initHour;
	int endHour;
	int initMinute;
	int endMinute;
	
	public DTOServiceRange(int dayOfTheWeek, int initHour, int endHour, int initMinute, int endMinute) {
		super();
		this.dayOfTheWeek = dayOfTheWeek;
		this.initHour = initHour;
		this.endHour = endHour;
		this.initMinute = initMinute;
		this.endMinute = endMinute;
	}

	public int getDayOfTheWeek() {
		return dayOfTheWeek;
	}

	public void setDayOfTheWeek(int dayOfTheWeek) {
		this.dayOfTheWeek = dayOfTheWeek;
	}

	public int getInitHour() {
		return initHour;
	}

	public void setInitHour(int initHour) {
		this.initHour = initHour;
	}

	public int getEndHour() {
		return endHour;
	}

	public void setEndHour(int endHour) {
		this.endHour = endHour;
	}

	public int getInitMinute() {
		return initMinute;
	}

	public void setInitMinute(int initMinute) {
		this.initMinute = initMinute;
	}

	public int getEndMinute() {
		return endMinute;
	}

	public void setEndMinute(int endMinute) {
		this.endMinute = endMinute;
	}	
}
