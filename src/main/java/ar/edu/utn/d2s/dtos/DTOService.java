package ar.edu.utn.d2s.dtos;

import java.util.List;

public class DTOService {

	String name;
	List<DTOServiceRange> ranges;
	
	public DTOService(String name, List<DTOServiceRange> ranges) {
		super();
		this.name = name;
		this.ranges = ranges;
	}

	public String getName() {
		return name;
	}

	public void setName(String serviceName) {
		this.name = serviceName;
	}

	public List<DTOServiceRange> getRanges() {
		return ranges;
	}

	public void setRanges(List<DTOServiceRange> ranges) {
		this.ranges = ranges;
	}
	
	//TODO: getters & setters validations
	
}
