package ar.edu.utn.d2s.dao.impl;

import org.springframework.stereotype.Repository;

import ar.edu.utn.d2s.dao.UserDAO;
import ar.edu.utn.d2s.model.User;

@Repository("userDAO")
public class UserDAOImpl extends AbstractDAOImpl<User, Integer> implements UserDAO{

	@Override
	protected Class<User> getEntityClass() {
		return User.class;
	}

}