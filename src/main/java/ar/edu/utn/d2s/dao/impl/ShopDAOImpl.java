package ar.edu.utn.d2s.dao.impl;

import org.springframework.stereotype.Repository;

import ar.edu.utn.d2s.dao.ShopDAO;
import ar.edu.utn.d2s.model.poi.Shop;

@Repository("shopDAO")
public class ShopDAOImpl extends AbstractDAOImpl<Shop, Integer> implements ShopDAO{

	@Override
	protected Class<Shop> getEntityClass() {
		return Shop.class;
	}

}
