package ar.edu.utn.d2s.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import ar.edu.utn.d2s.dao.AbstractDAO;
 
 
public abstract class AbstractDAOImpl<E , PK extends Serializable> implements AbstractDAO <E, PK> {

    @Autowired
	protected SessionFactory sessionFactory ;

    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    protected abstract Class<E> getEntityClass();

    public void save(E entity) {
        getSession().persist(entity);
    }

    public void delete(E entity) {
        getSession().delete(entity);
    }

    public void update(E entity) {
        getSession().update(entity);
    }

	@SuppressWarnings("unchecked")
	public E get(PK id){
       return (E) getSession().get(getEntityClass(), id);
	}

	@SuppressWarnings("unchecked")
	public List<E> findAll(){
       Criteria criteria = getSession().createCriteria(getEntityClass());
       criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
       return (List<E>) criteria.list();
	}

   @SuppressWarnings("unchecked")
   public List<E> findyByProperty(String propertyName, Object value){
       Criteria criteria = getSession().createCriteria(getEntityClass());
       criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
       criteria.add(Restrictions.eq(propertyName, value));
       return (List<E>) criteria.list();
   }
}
