package ar.edu.utn.d2s.dao;
 

import ar.edu.utn.d2s.model.poi.PointOfInterest;

public interface PoiDAO extends AbstractDAO<PointOfInterest,Integer>{
	 
}
