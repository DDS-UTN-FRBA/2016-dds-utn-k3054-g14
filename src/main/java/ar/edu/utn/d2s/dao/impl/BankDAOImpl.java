package ar.edu.utn.d2s.dao.impl;

import org.springframework.stereotype.Repository;

import ar.edu.utn.d2s.dao.BankDAO;
import ar.edu.utn.d2s.model.poi.Bank;

@Repository("bankDAO")
public class BankDAOImpl extends AbstractDAOImpl<Bank, Integer> implements BankDAO{

	@Override
	protected Class<Bank> getEntityClass() {
		return Bank.class;
	}

}
