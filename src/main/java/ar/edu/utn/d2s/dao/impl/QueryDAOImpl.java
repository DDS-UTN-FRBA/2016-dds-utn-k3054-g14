package ar.edu.utn.d2s.dao.impl;

import org.springframework.stereotype.Repository;

import ar.edu.utn.d2s.dao.QueryDAO;
import ar.edu.utn.d2s.model.Query;

@Repository("queryDAO")
public class QueryDAOImpl extends AbstractDAOImpl<Query, Integer> implements QueryDAO{

	@Override
	protected Class<Query> getEntityClass() {
		return Query.class;
	}

}

