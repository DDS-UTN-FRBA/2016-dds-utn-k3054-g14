package ar.edu.utn.d2s.dao.impl;

import org.springframework.stereotype.Repository;

import ar.edu.utn.d2s.dao.BusDAO;
import ar.edu.utn.d2s.model.poi.Bus;

@Repository("busDAO")
public class BusDAOImpl extends AbstractDAOImpl<Bus, Integer> implements BusDAO{

	@Override
	protected Class<Bus> getEntityClass() {
		return Bus.class;
	}

}
