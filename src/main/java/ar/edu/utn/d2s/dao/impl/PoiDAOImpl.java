package ar.edu.utn.d2s.dao.impl;

import org.springframework.stereotype.Repository;

import ar.edu.utn.d2s.dao.PoiDAO;
import ar.edu.utn.d2s.model.poi.PointOfInterest;

@Repository("poiDAO")
public class PoiDAOImpl extends AbstractDAOImpl<PointOfInterest, Integer> implements PoiDAO{

	@Override
	protected Class<PointOfInterest> getEntityClass() {
		return PointOfInterest.class;
	} 
}
