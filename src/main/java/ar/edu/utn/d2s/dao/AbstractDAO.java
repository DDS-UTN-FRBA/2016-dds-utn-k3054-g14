package ar.edu.utn.d2s.dao;

import java.io.Serializable;
import java.util.List;

public interface AbstractDAO <E, PK extends Serializable>{
    void  save(E newEntity);
    void delete(E entity);
    void update(E enity);
    E get(PK id);
    List<E> findAll();
    List<E> findyByProperty(String propertyName, Object value);
}
