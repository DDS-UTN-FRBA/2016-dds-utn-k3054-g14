package ar.edu.utn.d2s.dao;

import ar.edu.utn.d2s.model.poi.Bank;

public interface BankDAO extends AbstractDAO<Bank, Integer>{

}
