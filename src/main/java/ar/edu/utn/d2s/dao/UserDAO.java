package ar.edu.utn.d2s.dao;

import ar.edu.utn.d2s.model.User;

public interface UserDAO extends AbstractDAO<User, Integer>{

}

