package ar.edu.utn.d2s.dao.impl;

import org.springframework.stereotype.Repository;

import ar.edu.utn.d2s.dao.CgpDAO;
import ar.edu.utn.d2s.model.poi.CGP;

@Repository("cgpDAO")
public class CgpDAOImpl extends AbstractDAOImpl<CGP, Integer> implements CgpDAO{

	@Override
	protected Class<CGP> getEntityClass() {
		return CGP.class;
	}

}
