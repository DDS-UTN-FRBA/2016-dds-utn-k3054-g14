package ar.edu.utn.d2s.dao;

import ar.edu.utn.d2s.model.poi.Shop;
 
public interface ShopDAO extends AbstractDAO<Shop, Integer>{

}
