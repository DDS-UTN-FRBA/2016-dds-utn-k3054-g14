package ar;
 
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import ar.edu.utn.d2s.hibernate.HibernateConfiguration;
import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.services.UserService;
import ar.edu.utn.d2s.useractions.GenerateLog;
import ar.edu.utn.d2s.useractions.GenerateSearchReport;

public class AppMain {

	 public static void main( String[] args )
	    { 
		 	AbstractApplicationContext context = new AnnotationConfigApplicationContext(HibernateConfiguration.class);

		 	///*** user creation ***///
		 	
		 	User user = new User();
			GenerateLog logAction = new GenerateLog("logger"); 
			GenerateSearchReport searchReportAction = new GenerateSearchReport("Search Report");
			user.setName("Administrator");
			user.setSaveSearches(true);
			user.addUserActions(logAction);
			user.addUserActions(searchReportAction);
	    	
			//***//
			
	    	UserService userService = (UserService)context.getBean("userService");
	    	
	    	userService.save(user);
	    	
	    	userService.getById(user.getId());
	    	
	    	user.setName("new name");
	    	
	    	userService.update(user);
	    	
	    	userService.delete(user);
	    	
	    	context.close();
	    	
	    }
}
