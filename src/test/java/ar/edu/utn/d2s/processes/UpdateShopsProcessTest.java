package ar.edu.utn.d2s.processes;
 

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Before; 
import org.junit.Test;

import static org.junit.Assert.*;
import ar.edu.utn.d2s.exceptions.PointOfInterestFactoryException;
import ar.edu.utn.d2s.helpers.PointOfInterestDummyFactory;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.processes.ExecutionResult;
import ar.edu.utn.d2s.processes.UpdateShopsProcess;
import ar.edu.utn.d2s.repositories.InMemoryRepository;

public class UpdateShopsProcessTest {

	protected InMemoryRepository repository;
	protected UpdateShopsProcess shopProcess;
	private PointOfInterest point;
	private PointOfInterest point2;

	@Before
	public void setUp() throws Exception {

		this.repository = new InMemoryRepository(); // binding between interface and concrete

		this.point = PointOfInterestDummyFactory.createShop();

		this.point2 = PointOfInterestDummyFactory.createShop2();
		String filePath = new File("").getAbsolutePath();
		String path = filePath.concat("/src/main/java/ar/KeywordsFile.txt");
		 
	
		this.shopProcess = new UpdateShopsProcess(
				"Shop update process",path,
				repository);

	}
	

	@Test
	// update carrousel shop and create local2
	public void test1() throws FileNotFoundException, IOException,
			PointOfInterestFactoryException {
		
		this.repository.add(point);
		this.repository.add(point2);
		
		ExecutionResult result = shopProcess.execute();

		assertEquals(200, result.getCode().get(0).intValue()); // updated carrousell
		assertEquals(201, result.getCode().get(1).intValue()); // created local2
		assertEquals(3,this.repository.getAllLocal().size()); // 3 elements in repo
		assertTrue(this.repository.getAllLocal().get(2).getName().equals("Local2")); // 3rd element is local2
		
		
		this.repository.remove(point2);
		this.repository.remove(point);
	}
	
}
