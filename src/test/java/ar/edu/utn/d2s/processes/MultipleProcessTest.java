package ar.edu.utn.d2s.processes;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import ar.edu.utn.d2s.exceptions.PointOfInterestFactoryException;
import ar.edu.utn.d2s.helpers.PointOfInterestDummyFactory;
import ar.edu.utn.d2s.helpers.UserDummyFactory;
import ar.edu.utn.d2s.mockobjects.MockBajaPDI;
import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.processes.AddUserActionsProcess;
import ar.edu.utn.d2s.processes.AsyncProcess;
import ar.edu.utn.d2s.processes.DeactivatePDIProcess;
import ar.edu.utn.d2s.processes.ExecutionResult;
import ar.edu.utn.d2s.processes.MultipleExecution;
import ar.edu.utn.d2s.processes.UpdateShopsProcess;
import ar.edu.utn.d2s.repositories.InMemoryRepository;
import ar.edu.utn.d2s.repositories.UsersRepository;
import ar.edu.utn.d2s.useractions.Action;
import ar.edu.utn.d2s.useractions.ActionsByUser;
import ar.edu.utn.d2s.useractions.MailAdmin;

public class MultipleProcessTest {

	//for  add users actions 

	protected UsersRepository userRepo;
	protected List<ActionsByUser> newActions;
	protected AddUserActionsProcess addUserActionsProcess;
	protected Action actionMail;
	
	//fpr  deactivate pdi 
	
	protected InMemoryRepository repository;
	protected DeactivatePDIProcess deactivateProcess;
	
	//for update shops
	
	protected UpdateShopsProcess shopProcess;
	
	//for multiple process
	
	private List <AsyncProcess> processes;	
	private List <ExecutionResult> results;
	MultipleExecution multiple;
	
	private PointOfInterest busPoint;
	private PointOfInterest bankPoint;
	private PointOfInterest cgpPoint;
	private PointOfInterest shopPoint;
	
	
	@Before
	public void setUp() throws Exception {
		
		// for add users actions
		
		this.userRepo = new UsersRepository(); 
		this.newActions = new ArrayList<ActionsByUser>();
		
		User userT4 = UserDummyFactory.createTerminal4(); 
		
	 
		userRepo.add(userT4); //only has log action
		
		Set<Action> actions = new HashSet<Action>(); //created set of actions
		actionMail = new MailAdmin("Mail T1");
		actions.add(actionMail);	 //added an action to a set of actions
		
		ActionsByUser pairActionUser = new ActionsByUser(userT4, actions); //created a pair user/actions
		
		newActions.add(pairActionUser); //added to a list of users and actions to update
		
		this.addUserActionsProcess  = new AddUserActionsProcess("Update Actions per Users test",userRepo,newActions);
		
		
		//for deactivate pdi
		
		MockBajaPDI POIsListFromCABAInterface = new MockBajaPDI();

		this.repository = new InMemoryRepository(); // binding between interface
													// and concrete

		this.busPoint = PointOfInterestDummyFactory.createBus();
		this.bankPoint = PointOfInterestDummyFactory.createBank();
		this.cgpPoint = PointOfInterestDummyFactory.createCGP();
		this.shopPoint = PointOfInterestDummyFactory.createShop();


		this.deactivateProcess = new DeactivatePDIProcess(
				"Deactivate PDI Process", repository,
				POIsListFromCABAInterface.getData());
		
		//for update shops
		
		 
		this.shopProcess = new UpdateShopsProcess(
				"Shop update process",
				"/home/dds/git/2016-dds-utn-k3054-g14/src/main/java/ar/KeywordsFile.txt",
				repository);

		
		//and now for multiple process
		this.processes = new ArrayList<AsyncProcess>(); //create list of processes
		
		this.processes.add(addUserActionsProcess); // add them to the list
		this.processes.add(deactivateProcess);
		this.processes.add(shopProcess);
		
		this.results = new ArrayList<ExecutionResult>(); // create the list of results
		
		this.multiple = new MultipleExecution("Multiple Process test",processes);
		
 

	}
	
	@Test
	public void multipleTest() throws FileNotFoundException, IOException, PointOfInterestFactoryException{
		
		

		this.repository.add(busPoint);
		this.repository.add(bankPoint);
		this.repository.add(cgpPoint);
		this.repository.add(shopPoint);
		
		
		results = multiple.executeAll();
		
		assertEquals(3,results.size()); //3 results, one per process executed.
		
		
		assertEquals(200,results.get(0).getCode().get(0).intValue());
		assertEquals(200,results.get(1).getCode().get(0).intValue());
		assertEquals(200,results.get(1).getCode().get(1).intValue());
		assertEquals(2,results.get(2).getCode().size());
		 
		assertEquals("OK. Carrousel Clothes Shop Updated.", results.get(2).getStatus().get(0));
	 
		assertEquals("OK. Local2 Shop Created.", results.get(2).getStatus().get(1));
	
		
		this.repository.remove(busPoint);
		this.repository.remove(bankPoint);
		this.repository.remove(cgpPoint);
		this.repository.remove(shopPoint);
	}
	
	
}
