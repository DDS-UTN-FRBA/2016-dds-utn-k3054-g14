package ar.edu.utn.d2s.processes;

import static org.junit.Assert.assertEquals; 

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import ar.edu.utn.d2s.exceptions.PointOfInterestFactoryException;
import ar.edu.utn.d2s.helpers.PointOfInterestDummyFactory;
import ar.edu.utn.d2s.mockobjects.MockBajaPDI;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.processes.DeactivatePDIProcess;
import ar.edu.utn.d2s.processes.ExecutionResult;
import ar.edu.utn.d2s.repositories.InMemoryRepository;

public class DeactivatePDIProcessTest {

	protected InMemoryRepository repository;
	protected DeactivatePDIProcess deactivateProcess;
	private PointOfInterest busPoint;
	private PointOfInterest bankPoint;
	private PointOfInterest cgpPoint;
	private PointOfInterest shopPoint;

	
	@Before
	public void setUp() throws Exception {

		MockBajaPDI POIsListFromCABAInterface = new MockBajaPDI();

		this.repository = new InMemoryRepository(); // binding between interface
													// and concrete

		this.busPoint = PointOfInterestDummyFactory.createBus();
		this.bankPoint = PointOfInterestDummyFactory.createBank();
		this.cgpPoint = PointOfInterestDummyFactory.createCGP();
		this.shopPoint = PointOfInterestDummyFactory.createShop();

		

		this.deactivateProcess = new DeactivatePDIProcess(
				"Deactivate PDI Process", repository,
				POIsListFromCABAInterface.getData());

	}

	@Test
	// this test deactivates 2 POIS (carrousel clothes & bus 123) that came from the external service (mocked)  
	// from the 4 pois that are stored in the repo
	public void test1() throws FileNotFoundException, IOException,
			PointOfInterestFactoryException {

		
		this.repository.add(busPoint);
		this.repository.add(bankPoint);
		this.repository.add(cgpPoint);
		this.repository.add(shopPoint);
		
		
		assertEquals(4, this.repository.getAllActives().size()); //all POIs are active	
		
		ExecutionResult result = deactivateProcess.execute();

		assertEquals(2, this.repository.getAllActives().size());	 //only 2 POIs are active and 2 where deactivated
		
		assertEquals(2, result.getCode().size()); //only 2 POIs are active and 2 where deactivated with code 200

		assertEquals(200, result.getCode().get(0).intValue()); 

		assertEquals(200, result.getCode().get(1).intValue());
		
		this.repository.remove(shopPoint);
		this.repository.remove(cgpPoint);
		this.repository.remove(bankPoint);
		this.repository.remove(busPoint);
	}

}
