package ar.edu.utn.d2s.processes;

 

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import ar.edu.utn.d2s.helpers.UserDummyFactory;
import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.processes.AddUserActionsProcess;
import ar.edu.utn.d2s.processes.ExecutionResult;
import ar.edu.utn.d2s.repositories.UsersRepository;
import ar.edu.utn.d2s.useractions.Action;
import ar.edu.utn.d2s.useractions.ActionsByUser;
import ar.edu.utn.d2s.useractions.MailAdmin;



public class AddUserActionsProcessTest {
	
	protected UsersRepository userRepo;
	protected List<ActionsByUser> newActions;
	protected AddUserActionsProcess addUserActionsProcess;
	protected Action actionMail;
	
	@Before
	public void setUp() throws Exception {
		
		this.userRepo = new UsersRepository(); 
		this.newActions = new ArrayList<ActionsByUser>();
		
		User userT4 = UserDummyFactory.createTerminal4(); 
		
	 
		userRepo.add(userT4); //only has log action
		
		Set<Action> actions = new HashSet<Action>(); //created set of actions
		actionMail = new MailAdmin("Mail T1");
		actions.add(actionMail);	 //added an action to a set of actions
		
		ActionsByUser pairActionUser = new ActionsByUser(userT4, actions); //created a pair user/actions
		
		newActions.add(pairActionUser); //added to a list of users and actions to update
		
		this.addUserActionsProcess  = new AddUserActionsProcess("Update Actions per Users test",userRepo,newActions);
	}
	
	@Test
	public void test1 (){
		
		List<Action> beforeExecActions = new ArrayList<Action>(this.userRepo
				.getAllContent().get(0).getUserActions()); // list of old actions

		ExecutionResult result =this.addUserActionsProcess.execute();
		
		assertEquals(2, userRepo.getAllContent().get(0).getUserActions().size()); //size of list of new actions
		assertEquals(result.getCode().get(0).intValue(),200); //ok code 
		assertTrue( userRepo.getAllContent().get(0).getUserActions().contains(actionMail)); //contains the new action
		  
		assertTrue( userRepo.getAllContent().get(0).getUserActions().contains(beforeExecActions.get(0))); //contains the old action 
		
		
	}

}
