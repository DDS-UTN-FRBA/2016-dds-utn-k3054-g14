package ar.edu.utn.d2s.libraries;

import static org.junit.Assert.*;

import org.junit.Test;
import org.joda.time.LocalTime;

import ar.edu.utn.d2s.libraries.Schedule;
import ar.edu.utn.d2s.libraries.TimeInterval;
import ar.edu.utn.d2s.libraries.days.Days;

public class ScheduleTest {
	
	@Test
	public void createANewSchedule()
	{	
		Schedule schedule = new Schedule(
				Days.fromMondayToFriday(), 
				TimeInterval.list("10:00 to 15:00")
		);
		
		assertEquals(5, schedule.getDays().size());
		assertEquals(Days.getMonday(), schedule.getDays().get(0).intValue());
		assertEquals(1, schedule.getTimeIntervals().size());
		assertEquals(new LocalTime(10, 0), schedule.getTimeIntervals().get(0).getStartsAt());
	}
	

	
	@Test
	public void aScheduleKnowsIfItContainsATimeInterval()
	{
		Schedule schedule = new Schedule(
				Days.addMonday().addThursday().get(),
				TimeInterval.list("9:00 to 13:00", "14:00 to 18:00")
		);
		
		assertTrue(schedule.contains(Days.getMonday(), new LocalTime(10, 0, 0)));
		assertFalse(schedule.contains(Days.getMonday(), new LocalTime(14, 0, 0)));
		assertFalse(schedule.contains(Days.getWednesday(), new LocalTime(10, 0, 0)));		
		assertFalse(schedule.contains(Days.getThursday(), new LocalTime(19, 0, 0)));
	}
}
