package ar.edu.utn.d2s.libraries;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import ar.edu.utn.d2s.libraries.days.Days;

public class DaysTest {
	
	int monday = Days.getMonday();
	int tuesday = Days.getTuesday();
	int wednesday = Days.getWednesday();
	int thursday = Days.getThursday();
	int friday = Days.getFriday();
	int saturday = Days.getSaturday();
	int sunday = Days.getSunday();

	@Test
	public void createAListOfDaysFromMondayToFriday()
	{
		List<Integer> days = Days.fromMondayToFriday();
		
		assertTrue(days.contains(monday));
		assertTrue(days.contains(tuesday));
		assertTrue(days.contains(wednesday));
		assertTrue(days.contains(thursday));
		assertTrue(days.contains(friday));
		assertFalse(days.contains(saturday));
		assertFalse(days.contains(sunday));
	}
	
	@Test
	public void createAListOfDaysFromMondayToSaturday()
	{
		List<Integer> days = Days.fromMondayToSaturday();
		
		assertTrue(days.contains(monday));
		assertTrue(days.contains(tuesday));
		assertTrue(days.contains(wednesday));
		assertTrue(days.contains(thursday));
		assertTrue(days.contains(friday));
		assertTrue(days.contains(saturday));
		assertFalse(days.contains(sunday));
	}
	
	@Test
	public void createAListOfSpecificDays()
	{
		List<Integer> days = Days.addMonday().addThursday().addSunday().get();
		
		assertTrue(days.contains(monday));
		assertFalse(days.contains(tuesday));
		assertFalse(days.contains(wednesday));
		assertTrue(days.contains(thursday));
		assertFalse(days.contains(friday));
		assertFalse(days.contains(saturday));
		assertTrue(days.contains(sunday));
	}
}
