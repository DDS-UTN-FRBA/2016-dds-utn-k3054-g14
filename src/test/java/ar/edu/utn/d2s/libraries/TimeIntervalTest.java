package ar.edu.utn.d2s.libraries;

import java.util.List;

import org.joda.time.LocalTime;
import org.junit.Test;

import ar.edu.utn.d2s.libraries.TimeInterval;
import static org.junit.Assert.*;


public class TimeIntervalTest {

	@Test
	public void instanceNewTimeIntervalByPassingTwoLocalTimes()
	{	
		LocalTime startsAt = new LocalTime(10, 0, 0);
		LocalTime endsAt = new LocalTime(15, 0, 0);
		
		TimeInterval timeInterval = new TimeInterval(startsAt, endsAt);
		
		assertEquals(startsAt, timeInterval.getStartsAt());
		assertEquals(endsAt, timeInterval.getEndsAt());

	}
	
	@Test
	public void instanceANewTimeIntervalByPassingHoursAndMinutes()
	{
		LocalTime startsAt = new LocalTime(9, 0);
		LocalTime endsAt = new LocalTime(13, 0);
		
		TimeInterval timeInterval = new TimeInterval(9,0,13,0);
		
		assertEquals(startsAt, timeInterval.getStartsAt());
		assertEquals(endsAt, timeInterval.getEndsAt());
	}
	
	@Test
	public void instanceANewTimeIntervalByPassingTwoStrings()
	{
		TimeInterval timeInterval = new TimeInterval("9:00", "15:00");
		
		assertEquals(new LocalTime(9,0), timeInterval.getStartsAt());
		assertEquals(new LocalTime(15,0), timeInterval.getEndsAt());
	}
	
	@Test
	public void buildAListOfTimeIntervalsByPassingTwoTimeIntervalsAsStrings()
	{
		List<TimeInterval> list = TimeInterval.list("9:00 to 13:00", "14:00 to 18:00");
		
		assertEquals(2, list.size());
		assertEquals(new LocalTime(9,0), list.get(0).getStartsAt());
		assertEquals(new LocalTime(13,0), list.get(0).getEndsAt());
		assertEquals(new LocalTime(14,0), list.get(1).getStartsAt());
		assertEquals(new LocalTime(18,0), list.get(1).getEndsAt());
	}
	
	@Test
	public void aTimeIntervalKnowsIfItContainsALocalTime()
	{
		TimeInterval timeInterval = new TimeInterval(10, 0, 15, 0);
		
		LocalTime time = new LocalTime(11, 0, 0);
		LocalTime otherTime = new LocalTime(18, 0, 0);
		
		assertTrue(timeInterval.contains(time));
		assertFalse(timeInterval.contains(otherTime));
	}
}
