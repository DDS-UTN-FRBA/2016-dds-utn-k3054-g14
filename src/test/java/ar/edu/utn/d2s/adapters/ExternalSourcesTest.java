package ar.edu.utn.d2s.adapters;

import static org.junit.Assert.*;

import org.joda.time.LocalTime;
import org.junit.Test;

import ar.edu.utn.d2s.adapters.BankAdapter;
import ar.edu.utn.d2s.adapters.CGPAdapter;
import ar.edu.utn.d2s.interfaces.ExternalSource;
import ar.edu.utn.d2s.mockobjects.MockBanks;
import ar.edu.utn.d2s.mockobjects.MockCGPs;
import ar.edu.utn.d2s.model.poi.Bank;
import ar.edu.utn.d2s.model.poi.CGP;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.providers.BankDataProvider;
import ar.edu.utn.d2s.providers.CGPDataProvider;

import java.util.List;

public class ExternalSourcesTest {
	
	// Bank External Source
	
	@Test
	public void getBanksFromExternalSource() throws Exception {
		
		// Bindings
		BankDataProvider provider = new MockBanks();
		ExternalSource source = new BankAdapter(provider);
		
		List<PointOfInterest> pointsOfInterest = source.getPointsOfInterest();
		
		// We assert we have 2 items in the collection
		assertEquals(2, pointsOfInterest.size());
		
		// We assert each item is a Bank
		pointsOfInterest.stream().forEach(point -> {
			assertTrue(point instanceof Bank);
		});
		
		Bank bankOne = (Bank) pointsOfInterest.get(0);
		Bank bankTwo = (Bank) pointsOfInterest.get(1);

		// We assert the amount of services each bank has
		assertEquals(5, bankOne.getServices().size());
		assertEquals(4, bankTwo.getServices().size());
	}
	
	@Test
	public void searchBanksByNameAndService() throws Exception {
		// Bindings
		BankDataProvider provider = new MockBanks();
		ExternalSource source = new BankAdapter(provider);
		
		// 1st search: one POI found
		List<PointOfInterest> pointsSearch1 = source.search("Banco de la Plaza", "créditos");
		assertEquals(1, pointsSearch1.size());
		assertTrue(pointsSearch1.get(0) instanceof Bank);
		assertEquals("Banco de la Plaza", pointsSearch1.get(0).getName());
		assertTrue(pointsSearch1.get(0).isAvailable(1, new LocalTime(10, 00), "créditos"));
		
		// 2nd search: 0 POIs found, nonexistent bank name
		List<PointOfInterest> pointsSearch2 = source.search("NonExistent", "créditos");
		assertEquals(0, pointsSearch2.size());
		
		// 3rd search: 2 POIs found
		List<PointOfInterest> pointsSearch3 = source.search("Banco de la Plaza", "extracciones");
		assertEquals(2, pointsSearch3.size());
		assertTrue(pointsSearch3.get(0) instanceof Bank);
		assertEquals("Banco de la Plaza", pointsSearch3.get(0).getName());
		assertTrue(pointsSearch3.get(0).isAvailable(1, new LocalTime(10, 00), "extracciones"));
		assertTrue(pointsSearch3.get(1) instanceof Bank);
		assertEquals("Banco de la Plaza", pointsSearch3.get(1).getName());
		assertTrue(pointsSearch3.get(1).isAvailable(1, new LocalTime(10, 00), "extracciones"));
	}
	
	// CGP External Source
	
	@Test
	public void getCGPsFromExternalSource() throws Exception {
		
		// Bindings
		CGPDataProvider provider = new MockCGPs();
		ExternalSource source = new CGPAdapter(provider);
		
		List<PointOfInterest> pointsOfInterest = source.getPointsOfInterest();
		
		// We assert we have 2 items in the collection
		assertEquals(2, pointsOfInterest.size());
		
		// We assert each item is a CGP
		pointsOfInterest.stream().forEach(point -> {
			assertTrue(point instanceof CGP);
		});
		
		CGP cgpOne = (CGP) pointsOfInterest.get(0);
		CGP cgpTwo = (CGP) pointsOfInterest.get(1);
		
		// Assertions cgpOne
		assertEquals(1, cgpOne.getServices().size());
		assertEquals(3, cgpOne.getServices().get(0).getSchedule().getDays().size());
		assertEquals(2, cgpOne.getCommune());
		assertEquals("Recoleta", cgpOne.getName());
		assertEquals("Uriburu", cgpOne.getDirection().getCallePrincipal());
		assertEquals(1022, cgpOne.getDirection().getNumero());
		
		// Assertions cgpTwo
		assertEquals(1, cgpTwo.getServices().size());
		assertEquals(3, cgpTwo.getServices().get(0).getSchedule().getDays().size());
		assertEquals(14, cgpTwo.getCommune());
		assertEquals("Palermo", cgpTwo.getName());
		assertEquals("Beruti", cgpTwo.getDirection().getCallePrincipal());
		assertEquals(3325, cgpTwo.getDirection().getNumero());
	}
	
	@Test
	public void searchCGPsByName() throws Exception {
		// Bindings
		CGPDataProvider provider = new MockCGPs();
		ExternalSource source = new CGPAdapter(provider);
		
		// 1st search: one POI found
		List<PointOfInterest> pointsSearch1 = source.search("Uriburu");
		assertEquals(1, pointsSearch1.size());
		assertTrue(pointsSearch1.get(0) instanceof CGP);
		assertEquals(2, ((CGP) pointsSearch1.get(0)).getCommune());
		assertTrue(pointsSearch1.get(0).isAvailable(1, new LocalTime(12, 00), "Deposits"));
		
		// 2nd search: 0 POIs found, unexistent bank name
		List<PointOfInterest> pointsSearch2 = source.search("NonExistent");
		assertEquals(0, pointsSearch2.size());
		
		// 3rd search:2 other POIs found
		List<PointOfInterest> pointsSearch3 = source.search("Beruti");
		assertEquals(1, pointsSearch3.size());
		assertTrue(pointsSearch3.get(0) instanceof CGP);
		assertEquals(14, ((CGP) pointsSearch3.get(0)).getCommune());
		assertTrue(pointsSearch3.get(0).isAvailable(1, new LocalTime(12, 00), "Transfers"));
	}
}
