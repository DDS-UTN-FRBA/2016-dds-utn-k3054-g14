package ar.edu.utn.d2s.repositories;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import ar.edu.utn.d2s.adapters.BankAdapter;
import ar.edu.utn.d2s.adapters.CGPAdapter;
import ar.edu.utn.d2s.interfaces.ExternalSource;
import ar.edu.utn.d2s.interfaces.Searcher;
import ar.edu.utn.d2s.mockobjects.MockBanks;
import ar.edu.utn.d2s.mockobjects.MockCGPs;
import ar.edu.utn.d2s.model.poi.Bank;
import ar.edu.utn.d2s.model.poi.CGP;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.repositories.ExternalRepository;

public class ExternalRepositoryTest {
	
	protected Searcher repository;
		
	@Before
	public void setUp() throws Exception {
		List<ExternalSource> sources = new ArrayList<ExternalSource>();
		sources.add(new BankAdapter(new MockBanks()));
		sources.add(new CGPAdapter(new MockCGPs()));
		
		this.repository = new ExternalRepository(sources);
	}
	

	  
	@Test 
	public void searchPointsInExternalSourcesAndThenMakeTheSameSearchHitingTheCacheInstead() throws Exception {
		// We'll make the same search twice. 
		// Asserting the second time we actually hit the cache
		
		List<PointOfInterest> results = this.repository.search("Banco de la Plaza", "extracciones"); // 2 banks
		
		assertEquals(2, results.size());
		results.stream().forEach(point -> {
			assertTrue(point instanceof Bank);
		});
		assertEquals(0, ((ExternalRepository) this.repository).getHitsInCache());
		
		// Now we hit again the same search.
		List<PointOfInterest> results2 = this.repository.search("Banco de la Plaza", "extracciones"); // 2 banks
		
		assertEquals(2, results2.size());
		results2.stream().forEach(point -> {
			assertTrue(point instanceof Bank);
		});
		assertEquals(1, ((ExternalRepository) this.repository).getHitsInCache());

	}
	 
	@Test 
	public void searchPointsInExternalSources () throws Exception {
		// We'll make the same search twice. 
		// Asserting the second time we actually hit the cache
		
		List<PointOfInterest> results = this.repository.search("Recoleta"); // 1 cgp searched by zone
		
		assertEquals(1, results.size());
		results.stream().forEach(point -> {
			assertTrue(point instanceof CGP);
		});
		assertEquals(0, ((ExternalRepository) this.repository).getHitsInCache());
		
		// Now we hit again the same search.
		List<PointOfInterest> results2 = this.repository.search("Uriburu 1022"); // 1 cgp searched by adress
		
		assertEquals(0, results2.size());
		results2.stream().forEach(point -> {
			assertTrue(point instanceof CGP);
		});
		
		assertEquals(0, ((ExternalRepository) this.repository).getHitsInCache());

	}
}
