package ar.edu.utn.d2s.repositories;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import ar.edu.utn.d2s.helpers.PointOfInterestDummyFactory;
import ar.edu.utn.d2s.interfaces.PointOfInterestRepository;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.repositories.InMemoryRepository;
import ar.edu.utn.d2s.services.PoiService;

public class InMemoryRepositoryTest {
	
	protected PointOfInterestRepository repository;
	
	PointOfInterest busPoint;
	PointOfInterest bankPoint;
	PointOfInterest cgpPoint;
	PointOfInterest shopPoint;
	
	@Autowired
	private PoiService poiService;
	
	@Before
	public void setUp() throws Exception {
		this.repository = new InMemoryRepository(); // binding between interface and concrete
		
		busPoint = PointOfInterestDummyFactory.createBus();
		bankPoint = PointOfInterestDummyFactory.createBank();
		cgpPoint = PointOfInterestDummyFactory.createCGP();
		shopPoint = PointOfInterestDummyFactory.createShop();
	}

	@Test
	public void addAPointOfInterest() throws Exception
	{
		PointOfInterest point = PointOfInterestDummyFactory.createBus();
		
		this.repository.add(point);
		
		assertEquals(1, this.repository.count());
		
		this.repository.remove(point);
	}
	
	public void theSamePointWontBeAddedTwice() throws Exception
	{
		PointOfInterest point = PointOfInterestDummyFactory.createCGP();
			 
		this.repository.add(point);
		this.repository.add(point);
		
		assertEquals(1, this.repository.count());
		assertEquals(1, poiService.findAll().size());
		
		this.repository.remove(point);
	}
	
	@Test
	public void deleteAPointOfInterest() throws Exception 
	{
		PointOfInterest point = PointOfInterestDummyFactory.createBus();
		
		this.repository.add(point);
		
		assertEquals(1, this.repository.count());
		
		this.repository.remove(point);
		
		assertEquals(0, this.repository.count());
	}
	
	@Test
	public void searchPointOfInterest() throws Exception
	{
		
	
		this.repository.add(busPoint);
		this.repository.add(bankPoint);
		this.repository.add(cgpPoint);
		this.repository.add(shopPoint);
		
		List<PointOfInterest> pointsMatching132 = this.repository.search("132");
		assertEquals(1, pointsMatching132.size());
		assertEquals(pointsMatching132.get(0).getId(),busPoint.getId());
		
		this.repository.remove(busPoint);
		this.repository.remove(bankPoint);
		this.repository.remove(cgpPoint);
		this.repository.remove(shopPoint);
	}
	
	@Test
	public void searchPointOfInterestRecoleta() throws Exception
	{
	
		this.repository.add(busPoint);
		this.repository.add(bankPoint);
		this.repository.add(cgpPoint);
		this.repository.add(shopPoint);
		
		List<PointOfInterest> pointsMatchingRecoleta= this.repository.search("Rentas");
		assertEquals(1, pointsMatchingRecoleta.size());
		assertEquals(pointsMatchingRecoleta.get(0).getId(),cgpPoint.getId());

		this.repository.remove(busPoint);
		this.repository.remove(bankPoint);
		this.repository.remove(cgpPoint);
		this.repository.remove(shopPoint);
	}
	

	@Test
	public void searchActivePointOfInterest() throws Exception
	{
		
		this.repository.add(busPoint);
		this.repository.add(bankPoint);
		this.repository.add(cgpPoint);
		this.repository.add(shopPoint);
		
		 
		assertEquals(4, this.repository.getAllActives().size()); 

		this.repository.remove(busPoint);
		this.repository.remove(bankPoint);
		this.repository.remove(cgpPoint);
		this.repository.remove(shopPoint);
	}
}

