package ar.edu.utn.d2s.helpers;

import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.useractions.GenerateLog;
import ar.edu.utn.d2s.useractions.GenerateSearchReport;
import ar.edu.utn.d2s.useractions.MailAdmin;

public class UserDummyFactory {

	
	static GenerateLog logAction; 
	static GenerateSearchReport searchReportAction;
	static MailAdmin mailAdmin;
	
	public static User createAdmin(){
		
		User admin = new User();
		
		logAction = new GenerateLog("logger"); 
		searchReportAction = new GenerateSearchReport("Search Report");
		
		admin.setName("Administrator");
		admin.setSaveSearches(true);
		admin.setIsAdmin(true);
		admin.addUserActions(logAction);
		admin.addUserActions(searchReportAction);
		
		return admin;
		
	}
	
	public static User createTerminal1(){
		
		User terminal = new User();
		logAction = new GenerateLog("log1"); 
		searchReportAction = new GenerateSearchReport("Search Report 1");
		mailAdmin = new MailAdmin("Mail 1");
		
		terminal.setName("Terminal 1");
		terminal.setSaveSearches(true);
		terminal.addUserActions(searchReportAction);
		terminal.addUserActions(logAction);
		//terminal.addUserActions(mailAdmin);
		
		return terminal;
	}

	
	public static User createTerminal2() {
		
		User terminal = new User();
		logAction = new GenerateLog("log2"); 
		searchReportAction = new GenerateSearchReport("Search Report 2");
		mailAdmin = new MailAdmin("Mail 2");
		
		terminal.setName("Terminal 2");
		terminal.setSaveSearches(true);
		terminal.addUserActions(searchReportAction);
		terminal.addUserActions(logAction);
		
		//terminal.addUserActions(mailAdmin);
		
		return terminal;
	}

	public static User createTerminal3() {
		
		User terminal = new User();
		logAction = new GenerateLog("log3"); 
		searchReportAction = new GenerateSearchReport("Search Report 3");
		mailAdmin = new MailAdmin("Mail 3");
		
		terminal.setName("Terminal 3");
		terminal.setSaveSearches(true);
		terminal.addUserActions(searchReportAction);
		terminal.addUserActions(logAction);
		//terminal.addUserActions(mailAdmin);
		
		return terminal;
	}
	
public static User createTerminal4() {
		
		User terminal = new User();
		logAction = new GenerateLog("log4"); 
		 
		
		terminal.setName("Terminal 4");
		 
		terminal.addUserActions(logAction);
		 
		
		return terminal;
	}

}
