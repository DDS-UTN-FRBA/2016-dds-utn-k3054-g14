package ar.edu.utn.d2s.helpers;

import java.util.ArrayList;
import java.util.List;

import ar.edu.utn.d2s.libraries.Keyword;
import ar.edu.utn.d2s.libraries.Schedule;
import ar.edu.utn.d2s.libraries.TimeInterval;
import ar.edu.utn.d2s.libraries.days.Days;
import ar.edu.utn.d2s.model.BusStop;
import ar.edu.utn.d2s.model.Category;
import ar.edu.utn.d2s.model.Direction;
import ar.edu.utn.d2s.model.Point;
import ar.edu.utn.d2s.model.Service;
import ar.edu.utn.d2s.model.poi.Bank;
import ar.edu.utn.d2s.model.poi.Bus;
import ar.edu.utn.d2s.model.poi.CGP;
import ar.edu.utn.d2s.model.poi.Shop;

/**
 * This is not a test class.
 * Just a helper class to create POIs with dummy data
 */
public class PointOfInterestDummyFactory {

	public static Shop createShop() throws Exception 
	{	
		Schedule schedule = new Schedule(Days.fromMondayToSaturday(), TimeInterval.list("10:00 to 13:00", "17:00 to 20:00"));
		Category category = new Category();
		category.setName("Indumentaria infantil");
		category.setProximity(2.0);
		
		Direction direction = new Direction("Av. Corrientes", "Av. Callao", "Riobamba", 1812, 1, 'N', 1, 1045, "CABA", "Microcentro", "Buenos Aires", "Argentina");
		Point coordinates = new Point (-34.604408, -58.392684);
		List<Keyword> keywords = new ArrayList<Keyword>();
		
		keywords.add(new Keyword("Uniformes escolares"));
		keywords.add(new Keyword("Comuniones"));
		keywords.add(new Keyword("Baile"));
		keywords.add(new Keyword("Ropa para bebe"));
		
		Shop shop = new Shop(schedule, category, "Carrousel Clothes", coordinates, direction, keywords);
		
		return shop;
	}
	
	public static CGP createCGP() throws Exception 
	{	
		Schedule schedule = new Schedule(Days.fromMondayToFriday(), TimeInterval.list("9:30 to 15:30"));		
		
		List<Service> services = Service.list(new Service("Rentas", schedule));	

		ArrayList<Point> points = new ArrayList<Point>();
		points.add(new Point(-34.599771, -58.382430));
		points.add(new Point(-34.595761, -58.387172));
		points.add(new Point(-34.600142, -58.391571));
		points.add(new Point(-34.603921, -58.386550));

		Point coordinates = new Point(-34.600123, -58.386828);
		Direction direction = new Direction("Uruguay", "Av. Cordoba", "Viamonte", 740, 1, 'N', 1, 1015, "CABA", "Puerto Madero", "Buenos Aires", "Argentina");
		
		return new CGP("CGP 1", coordinates, direction, 1, points, services);
	}
	
	public static Bank createBank() throws Exception
	{
		Schedule schedule = new Schedule(Days.fromMondayToFriday(),TimeInterval.list("10:00 to 15:00"));
		List<Service> services = Service.list(new Service("Atencion Al Cliente", schedule));
		Direction direction = new Direction("Av.Rivadavia", "Pedernera", "Rodrigo Ingarte", 7000, 1, 'N', 1, 1406, "CABA", "Flores", "Buenos Aires", "Argentina");
		Point coordinates = new Point(-34.629100, -58.463573);
		
		return new Bank("Cajero Banco de La Nación Argentina", coordinates, direction, schedule, services,"Flores","Manager");
	}
	

	public static Bank createBank2() throws Exception
	{
		Schedule schedule = new Schedule(Days.fromMondayToFriday(),TimeInterval.list("10:00 to 15:00"));
		List<Service> services = Service.list(new Service("depósitos", schedule));
		Direction direction = new Direction("Av.Rivadavia", "Pedernera", "Rodrigo Ingarte", 7000, 1, 'N', 1, 1406, "CABA", "Flores", "Buenos Aires", "Argentina");
		Point coordinates = new Point(-34.629100, -58.463573);
		
		return new Bank("Banco de la Plaza", coordinates, direction, schedule, services,"Flores","Manager");
	}
	
	public static Bus createBus() throws Exception 
	{
		List<BusStop> busStops = BusStop.list(new Point(-34.629032, -58.463387), new Point(-34.627973, -58.459857));
		Point coordinates = new Point(-34.64,-58.45); // bus terminal 132
		Direction direction = new Direction("Av. Varela", 1628); // bus terminal 132
		
		return new Bus("Linea 132", coordinates, direction, 132, busStops);
	}

	public static Shop createShop2() throws Exception {
		
		Schedule schedule = new Schedule(Days.fromMondayToSaturday(), TimeInterval.list("10:00 to 13:00", "17:00 to 20:00"));
		Category category = new Category();
		category.setName("Indumentaria Adultos");
		category.setProximity(2.0);
		Direction direction = new Direction("Av. Corrientes 2", "Av. Callao 2", "Riobamba 2", 1812, 1, 'N', 1, 1045, "CABA", "Microcentro", "Buenos Aires", "Argentina");
		Point coordinates = new Point (-37.604408, -59.392684);
		List<Keyword> keywords = new ArrayList<Keyword>();
		
		keywords.add(new Keyword("Uniformes escolares"));
		keywords.add(new Keyword("Comuniones"));
		keywords.add(new Keyword("Baile"));
		keywords.add(new Keyword("Ropa para bebe"));
		
		Shop shop = new Shop(schedule, category, "Banco de la plaza SHOP", coordinates, direction, keywords);
 
		
		return shop;
	}
}
