package ar.edu.utn.d2s.userActions;


import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import ar.edu.utn.d2s.exceptions.user.ActionFailedException;
import ar.edu.utn.d2s.exceptions.user.UserCanNotReceiveEmailNotifException;
import ar.edu.utn.d2s.helpers.PointOfInterestDummyFactory;
import ar.edu.utn.d2s.helpers.UserDummyFactory;
import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.model.poi.PointOfInterest;

public class userActionsTest {
	
	private User admin;
	private User terminal; 
	
	@Before
	public void setUp() throws Exception {
		
	
		this.admin = UserDummyFactory.createAdmin();
		this.terminal = UserDummyFactory.createTerminal1();
	}

	@Test
	public void executeAdminTasksTest() throws Exception 
	{					
		DateTime initDateTime=DateTime.now();
		DateTime endDateTime=DateTime.now().plusHours(2);
		
		List<PointOfInterest> pointsMatching = new ArrayList<PointOfInterest>();
		pointsMatching.add(PointOfInterestDummyFactory.createBus());
		
		//admin has 2 actions: log info & save searches and takes 2 hours to run
		admin.executeActions("132", pointsMatching, initDateTime,endDateTime);
		
		assertEquals(admin.getSearchesRepo().count(),1);
	}
	
	@Test
	public void executeTerminalTasksTest() throws UserCanNotReceiveEmailNotifException, ActionFailedException 
	{					
		DateTime initDateTime=DateTime.now();
		DateTime endDateTime=DateTime.now().plusHours(1);
		List<PointOfInterest> pointsMatching = new ArrayList<PointOfInterest>();
		terminal.executeActions("133", pointsMatching, initDateTime,endDateTime);
		//terminal has 1 action: mail to admin and takes 1 hour to run
		assertEquals(admin.getSearchesRepo().count(),0);
		
	}
	/*
	@Test
	public void executNotActiveAction()throws UserCanNotReceiveEmailNotifException, ActionFailedException
	{
		
		TimeInterval interval = new TimeInterval(0, 0, 10, 10);
		List<PointOfInterest> pointsMatching = new ArrayList<PointOfInterest>();
		admin.executeActions("133", pointsMatching, interval);
		//terminal has 1 action: mail to admin
		assertEquals(admin.getSearchesRepo().count(),0);
	}
	*/
	

}
