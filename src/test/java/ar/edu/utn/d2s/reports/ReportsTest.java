package ar.edu.utn.d2s.reports;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.utn.d2s.helpers.PointOfInterestDummyFactory;
import ar.edu.utn.d2s.helpers.UserDummyFactory;
import ar.edu.utn.d2s.interfaces.PointOfInterestRepository;
import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.reports.SearchResultsPerTerminalReport;
import ar.edu.utn.d2s.reports.SearchesByDatePerTerminalReport;
import ar.edu.utn.d2s.repositories.InMemoryRepository;
import ar.edu.utn.d2s.repositories.SearchResultsRepository;
import ar.edu.utn.d2s.repositories.UsersRepository;

public class ReportsTest {
	
	private User terminalN1;
	private User terminalN2;
	private User terminalN3;
	
	List<PointOfInterest> pointsMatching132;
	List<PointOfInterest> pointsMatchingCGP1;
	
	protected PointOfInterestRepository repository;
	protected UsersRepository usersRepo;
	protected SearchResultsRepository querysRepo;
	
	private PointOfInterest busPoint;
	private PointOfInterest bankPoint;
	private PointOfInterest cgpPoint;
	private PointOfInterest shopPoint;
	
	@Before
	public void setUp() throws Exception {

		this.repository = new InMemoryRepository(); 
		this.usersRepo = new UsersRepository();
		this.querysRepo= new SearchResultsRepository();
		
		this.terminalN1 = UserDummyFactory.createTerminal1();
		this.terminalN2 = UserDummyFactory.createTerminal2();
		this.terminalN3 = UserDummyFactory.createTerminal3();
		
		busPoint = PointOfInterestDummyFactory.createBus();
		busPoint.setName("busPoint reportstest");
		bankPoint = PointOfInterestDummyFactory.createBank();
		bankPoint.setName("bankp rt");
		cgpPoint = PointOfInterestDummyFactory.createCGP();
		cgpPoint.setName("cgp rt");
		shopPoint = PointOfInterestDummyFactory.createShop();
		shopPoint.setName("shop rt");
	
		
		
		pointsMatching132 = new ArrayList<PointOfInterest>();
	
	}
	
	@After
	public void tearDown() {
	
		
	}
	
	@Test
	public void terminalN1SearchesByDateReportTest() throws Exception {
		
		
		this.repository.add(busPoint);
		this.repository.add(bankPoint);
		this.repository.add(cgpPoint);
		this.repository.add(shopPoint);
		
		
		////The table created by the report contains 0 elements
		assertEquals(SearchesByDatePerTerminalReport.find(terminalN1).size(),0);
		
		//first search
		DateTime initDateTime= DateTime.now();
		pointsMatching132 = this.repository.search("132");
		
		DateTime endDateTime=DateTime.now().plusSeconds(10);
		
		terminalN1.executeActions("132", pointsMatching132, initDateTime, endDateTime);
		
		//The table created by the report contains 1 search in this date
		assertEquals(SearchesByDatePerTerminalReport.find(terminalN1).size(),1);
		
		//second search
		
		initDateTime= DateTime.now();
		pointsMatchingCGP1 = this.repository.search("CGP 1");		 
		endDateTime=DateTime.now().plusSeconds(10);
		
		terminalN1.executeActions("Puerto Madero", pointsMatchingCGP1, initDateTime, endDateTime);
		
		//The table created by the report contains 2 searches in this date
		assertEquals(SearchesByDatePerTerminalReport.find(terminalN1).size(),1);
		
		//third search (made a day before)

		initDateTime= DateTime.now().minusDays(1);
		pointsMatchingCGP1 = this.repository.search("CGP 1");		 
		endDateTime=DateTime.now().minusDays(1).plusSeconds(10);
		
		terminalN1.executeActions("Puerto Madero", pointsMatchingCGP1, initDateTime, endDateTime);
		
		//The table created by the report contains 2 searches in this date and 1 yesterday
		assertEquals(SearchesByDatePerTerminalReport.find(terminalN1).size(),2);
		
		this.repository.remove(this.shopPoint);
		this.repository.remove(this.cgpPoint);
		this.repository.remove(this.bankPoint);
		this.repository.remove(this.busPoint);

	}
	
 
	@Test
	public void resultsPerTerminalReportTest() throws Exception{
	
		
		this.repository.add(busPoint);
		this.repository.add(bankPoint);
		this.repository.add(cgpPoint);
		this.repository.add(shopPoint);
		
		////The table created by the report contains 0 elements
		assertEquals(SearchesByDatePerTerminalReport.find(terminalN1).size(),0);
		
		//first search terminal1
		DateTime initDateTime= DateTime.now();
		pointsMatching132 = this.repository.search("132");
		
		DateTime endDateTime=DateTime.now().plusSeconds(10);
		
		terminalN1.executeActions("132", pointsMatching132, initDateTime, endDateTime);
		
		//second search terminal1
		
		initDateTime= DateTime.now();
		pointsMatchingCGP1 = this.repository.search("Rentas");		 
		endDateTime=DateTime.now().plusSeconds(10);
		
		terminalN1.executeActions("Rentas", pointsMatchingCGP1, initDateTime, endDateTime);
			 
		//first search  terminal2 (made a day before)

		initDateTime= DateTime.now().minusDays(1);
		pointsMatchingCGP1 = this.repository.search("Rentas");		 
		endDateTime=DateTime.now().minusDays(1).plusSeconds(10);
		
		terminalN2.executeActions("Rentas", pointsMatchingCGP1, initDateTime, endDateTime);
		 
		//first search  terminal3 (made a day before)

		initDateTime= DateTime.now().minusDays(1);
		pointsMatchingCGP1 = this.repository.search("Depósitos");		 
		endDateTime=DateTime.now().minusDays(1).plusSeconds(10);
		
		terminalN3.executeActions("Rentas", pointsMatchingCGP1, initDateTime, endDateTime);
		
		//first search  terminal2 (made a day before)

		initDateTime= DateTime.now().minusDays(1);
		pointsMatchingCGP1 = this.repository.search("Rentas");		 
		endDateTime=DateTime.now().minusDays(1).plusSeconds(10);
		
		terminalN2.executeActions("Rentas", pointsMatchingCGP1, initDateTime, endDateTime);
				 
		
		////////
		
		usersRepo.add(terminalN1);
		usersRepo.add(terminalN2);
		usersRepo.add(terminalN3);
		
		assertEquals(SearchResultsPerTerminalReport.find(usersRepo.getAllContent()).size(),3);
		
		usersRepo.remove(terminalN1);
		usersRepo.remove(terminalN2);
		usersRepo.remove(terminalN3); 
		
		this.repository.remove(this.shopPoint);
		this.repository.remove(this.cgpPoint);
		this.repository.remove(this.bankPoint);
		this.repository.remove(this.busPoint);
	}
}
