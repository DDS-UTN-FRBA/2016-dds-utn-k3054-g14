package ar.edu.utn.d2s.mockobjects;

import java.util.ArrayList;
import java.util.List;

import ar.edu.utn.d2s.dtos.DTOCgp;
import ar.edu.utn.d2s.dtos.DTOService;
import ar.edu.utn.d2s.dtos.DTOServiceRange;
import ar.edu.utn.d2s.providers.CGPDataProvider;


public class MockCGPs implements CGPDataProvider {

	public  List<DTOCgp> getData()
	{	
		List<DTOCgp> cgpDTOs = new ArrayList<DTOCgp>();
		
		cgpDTOs.add(this.buildDTOCgp1());
		cgpDTOs.add(this.buildDTOCgp2());
	 
		return  cgpDTOs;
	}
	
	/**
	 * CGP 1 Recoleta
	 *
	 * @return
	 */
	private DTOCgp buildDTOCgp1() {
		List<DTOServiceRange> ranges = this.buildDTOServiceRanges();
		
		DTOService serviceDTO = new DTOService("Deposits", ranges);
		
		List<DTOService> serviceDTOs = new ArrayList<DTOService>();
		serviceDTOs.add(serviceDTO);
		
		return new DTOCgp(2, "Recoleta", "John Doe", "Uriburu 1022", "48012232", serviceDTOs);
	}
	
	/**
	 * CGP 2 Palermo
	 * 
	 * @return
	 */
	private DTOCgp buildDTOCgp2() {
		List<DTOServiceRange> ranges = this.buildDTOServiceRanges();
		
		DTOService serviceDTO = new DTOService("Transfers", ranges);
		
		List<DTOService> serviceDTOs = new ArrayList<DTOService>();
		serviceDTOs.add(serviceDTO);
		
		return new DTOCgp(14, "Palermo", "Mark Foo", "Beruti 3325", "48275957" , serviceDTOs);
	}
	
	/**
	 * Monday to Wednesday
	 * From 9 to 18 hs
	 * 
	 * @return
	 */
	private List<DTOServiceRange> buildDTOServiceRanges() {
		
		// 3 DTO Service Ranges
		DTOServiceRange serviceRangeDTO1 = new DTOServiceRange(1, 9, 0, 18, 0); // Monday from 9 to 18
		DTOServiceRange serviceRangeDTO2 = new DTOServiceRange(2, 9, 0, 18, 0); // Tuesday from 9 to 18
		DTOServiceRange serviceRangeDTO3 = new DTOServiceRange(3, 9, 0, 18, 0); // Wednesday from 9 to 18
		
		// We add them to the list
		List<DTOServiceRange> serviceRanges = new ArrayList<DTOServiceRange>();
		
		serviceRanges.add(serviceRangeDTO1);
		serviceRanges.add(serviceRangeDTO2);
		serviceRanges.add(serviceRangeDTO3);
		
		return serviceRanges;
	}
}
