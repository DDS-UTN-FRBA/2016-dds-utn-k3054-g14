package ar.edu.utn.d2s.mockobjects;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List; 

import ar.edu.utn.d2s.model.BajaPDI;
import ar.edu.utn.d2s.providers.BajaPDIProvider;

public class MockBajaPDI implements BajaPDIProvider{

	@Override
	public List<BajaPDI> getData() {
		
		List<BajaPDI> BajaPDI = new ArrayList<BajaPDI>();
		LocalDate date = LocalDate.now();
		BajaPDI.add(new BajaPDI("132",date));
		BajaPDI.add(new BajaPDI("Carrousel Clothes",date));
	 
		return BajaPDI;
	}

}
