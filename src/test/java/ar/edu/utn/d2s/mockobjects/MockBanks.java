package ar.edu.utn.d2s.mockobjects;

import ar.edu.utn.d2s.providers.BankDataProvider;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class MockBanks implements BankDataProvider {

	public JsonArray getData() {
 		JsonArray banks = new JsonArray();	
 		
 		// Bank 1
 		
		JsonObject Bank1 = new JsonObject();
		Bank1.addProperty("banco","Banco de la Plaza");
		Bank1.addProperty("x","-35.9338322");
		Bank1.addProperty("y","72.348353");
		Bank1.addProperty("sucursal","Avellaneda");
		Bank1.addProperty("gerente","Javier Loeschbor");
		
		JsonArray dataSets1 = new JsonArray();
		dataSets1.add(new JsonPrimitive("cobro cheques"));
		dataSets1.add(new JsonPrimitive("depósitos"));
		dataSets1.add(new JsonPrimitive("extracciones"));
		dataSets1.add(new JsonPrimitive("transferencias"));
		dataSets1.add(new JsonPrimitive("créditos"));
		
		Bank1.add("servicios",dataSets1);
		
		// Bank 2
		
		JsonObject Bank2 = new JsonObject();
		Bank2.addProperty("banco","Banco de la Plaza");
		Bank2.addProperty("x","-35.9345681");
		Bank2.addProperty("y","72.344546");
		Bank2.addProperty("sucursal","Caballito");
		Bank2.addProperty("gerente","Fabián Fantaguzzi");
		
		JsonArray dataSets2 = new JsonArray();	
		dataSets2.add(new JsonPrimitive("depósitos"));
		dataSets2.add(new JsonPrimitive("extracciones"));
		dataSets2.add(new JsonPrimitive("transferencias"));
		dataSets2.add(new JsonPrimitive("seguros"));		
		
		Bank2.add("servicios",dataSets2);
		
		// Attach both banks to the collection
		
		banks.add(Bank1);
		banks.add(Bank2);
		
		return banks;
	}
}