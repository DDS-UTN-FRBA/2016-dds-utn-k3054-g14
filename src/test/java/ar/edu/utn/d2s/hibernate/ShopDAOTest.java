package ar.edu.utn.d2s.hibernate;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired; 

import ar.edu.utn.d2s.helpers.PointOfInterestDummyFactory;
import ar.edu.utn.d2s.model.poi.Shop;
import ar.edu.utn.d2s.services.ShopService;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ShopDAOTest extends AbstractTest implements DAOTest{

	@Autowired
	private ShopService shopService;
	 
	private Shop shop; 
	private Shop shop2;  
	
	
	@Override
	public void init() throws Exception {
		
		this.shop = PointOfInterestDummyFactory.createShop(); 
		this.shop2 = PointOfInterestDummyFactory.createShop2();
		
		 
	}


	@Override
	@Test	
	public void test_1_Insert() throws Exception {
	
		shopService.save(shop);
		Assert.assertNotNull(shop);
		Assert.assertNotNull(shopService.getById(shop.getId()).getId()); 
		shopService.delete(shop);
		
		
	}

	@Override
	@Test	
	public void test_2_Update() throws Exception{
		
		
		shopService.save(shop2);
		shop2.setName("A New Name4");
		shopService.update(shop2);
		String name = shopService.getById(shop2.getId()).getName();
		Assert.assertEquals("A New Name4", name); 
	
		shopService.delete(shop2);
		
	}

	@Override
	@Test	
	public void test_3_Delete() throws Exception{
		
		shopService.save(shop);
		Assert.assertNotNull(shopService.getById(shop.getId()));
		shopService.delete(shop);
		Shop deleted = shopService.getById(shop.getId());
		Assert.assertNull(deleted);
		
	}

	@Override
	@Test	
	public void test_4_FindAll() throws Exception {		
		
		shopService.save(shop);  
		
		Assert.assertEquals(1, shopService.findAll().size());
					
		shopService.delete(shop);  
	}

	@Override
	@Test	
	public void test_5_FindByProperty() throws Exception{
		
		shopService.save(shop);  
		shop.setName("A New Name 2");
		shopService.update(shop);
		Assert.assertEquals(1, shopService.findByProperty("name", "A New Name 2").size());	 
		shopService.delete(shop);  
	
	}


	
}
