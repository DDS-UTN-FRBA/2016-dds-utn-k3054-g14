package ar.edu.utn.d2s.hibernate;
 
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import ar.edu.utn.d2s.helpers.PointOfInterestDummyFactory;
import ar.edu.utn.d2s.model.poi.CGP;
import ar.edu.utn.d2s.services.CGPService;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CgpDAOTest extends AbstractTest implements DAOTest{

	@Autowired
	private CGPService cgpService;
	
	private CGP cgp;
	
	@Override
	public void init() throws Exception {
		
		this.cgp = PointOfInterestDummyFactory.createCGP();

		cgp.setName("cgp daotest");
		 
	}

	
	@Override
	@Test	
	public void test_1_Insert() throws Exception {
		
		cgpService.save(cgp);
		Assert.assertNotNull(cgp);
		Assert.assertNotNull(cgpService.getById(cgp.getId()).getId()); 
		cgpService.delete(cgp);
	}

	@Override
	@Test	
	public void test_2_Update() throws Exception{
		
		cgpService.save(cgp);
		cgp.setName("A New Name");
		cgpService.update(cgp);
		String name = cgpService.getById(cgp.getId()).getName();
		Assert.assertEquals("A New Name", name); 
		cgpService.delete(cgp);
	}

	@Override
	@Test	
	public void test_3_Delete() throws Exception{
		
		cgpService.save(cgp);
		Assert.assertNotNull(cgp);
		Assert.assertNotNull(cgpService.getById(cgp.getId()).getId()); 
		cgpService.delete(cgp);
		CGP	 deleted = cgpService.getById(cgp.getId());
		Assert.assertNull(deleted);
	}

	@Override
	@Test	
	public void test_4_FindAll() throws Exception { 
	
		cgpService.save(cgp);
		Assert.assertEquals(1, cgpService.findAll().size());
		cgpService.delete(cgp);
	}

	@Override
	@Test	
	public void test_5_FindByProperty() throws Exception{
		cgpService.save(cgp);
		cgp.setName("A New Name 2");
		cgpService.update(cgp);
		Assert.assertEquals(1, cgpService.findByProperty("name", "A New Name 2").size());	
		cgpService.delete(cgp);
	}
	
	
	
}
