package ar.edu.utn.d2s.hibernate;
 
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import ar.edu.utn.d2s.helpers.PointOfInterestDummyFactory;
import ar.edu.utn.d2s.model.poi.Bank;
import ar.edu.utn.d2s.services.BankService;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BankDAOTest extends AbstractTest implements DAOTest{

	@Autowired
	private BankService bankService;
	
	private Bank bank;
	
	@Override
	public void init() throws Exception {
		
		this.bank = PointOfInterestDummyFactory.createBank();
		 
	}

	
	@Override
	@Test	
	public void test_1_Insert() throws Exception {
		
		bankService.save(bank);
		Assert.assertNotNull(bank);
		Assert.assertNotNull(bankService.getById(bank.getId()).getId()); 
		bankService.delete(bank);
	}

	@Override
	@Test	
	public void test_2_Update() throws Exception{
		
		bankService.save(bank);
		bank.setName("A New Name");
		bankService.update(bank);
		String name = bankService.getById(bank.getId()).getName();
		Assert.assertEquals("A New Name", name); 
		bankService.delete(bank);
	}

	@Override
	@Test	
	public void test_3_Delete() throws Exception{
		
		bankService.save(bank);
		Assert.assertNotNull(bank);
		Assert.assertNotNull(bankService.getById(bank.getId()).getId()); 
		bankService.delete(bank);
		Bank deleted = bankService.getById(bank.getId());
		Assert.assertNull(deleted);
		
	}

	@Override
	@Test	
	public void test_4_FindAll() throws Exception { 
		bankService.save(bank);
		Assert.assertEquals(1, bankService.findAll().size());
		bankService.delete(bank);
	}

	@Override
	@Test	
	public void test_5_FindByProperty() throws Exception{
		bankService.save(bank);
		bank.setName("new name 2");
		Assert.assertEquals(0, bankService.findByProperty("name", bank.getName()).size());	
		bankService.delete(bank);
	}
	
	
	
}
