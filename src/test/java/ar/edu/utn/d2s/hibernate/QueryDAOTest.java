package ar.edu.utn.d2s.hibernate;
 
import java.time.LocalTime;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired; 

import ar.edu.utn.d2s.helpers.UserDummyFactory;
import ar.edu.utn.d2s.model.Query;
import ar.edu.utn.d2s.services.QueryService;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class QueryDAOTest extends AbstractTest implements DAOTest{

	@Autowired
	private QueryService queryService;
	
	private Query query;
	
	@Override
	public void init() throws Exception {
		
		this.query = new Query();
		query.setDate(LocalTime.now().toString());
		query.setKeyword("keyword");
		query.setQtyResults(2);
		query.setSeekTime(new Long(10));
		query.setUser(UserDummyFactory.createAdmin());
				 
	}

	
	@Override
	@Test	
	public void test_1_Insert() throws Exception {
		
		queryService.save(query);
		Assert.assertNotNull(query);
		Assert.assertNotNull(queryService.getById(query.getId()).getId()); 
		queryService.delete(query);
	}

	@Override
	@Test	
	public void test_2_Update() throws Exception{
		
		queryService.save(query);
		query.setQtyResults(20);
		queryService.update(query);
		Integer results = queryService.getById(query.getId()).getQtyResults();
		Assert.assertEquals(20, results.intValue()); 
		queryService.delete(query);
	}

	@Override
	@Test	
	public void test_3_Delete() throws Exception{
		
		queryService.save(query);
		Assert.assertNotNull(query);
		Assert.assertNotNull(queryService.getById(query.getId()).getId()); 
		queryService.delete(query);
		Query	 deleted = queryService.getById(query.getId());
		Assert.assertNull(deleted);
		
	}

	@Override
	@Test	
	public void test_4_FindAll() throws Exception { 
		Assert.assertEquals(0, queryService.findAll().size()); 
		queryService.save(query);
		Assert.assertEquals(1, queryService.findAll().size()); 
		queryService.delete(query);
	}

	@Override
	@Test	
	public void test_5_FindByProperty() throws Exception{
		queryService.save(query);
		query.setQtyResults(25);
		queryService.update(query);
		Assert.assertEquals(1, queryService.findByProperty("sizeResults", 25).size());	
		queryService.delete(query);
	}
	
	
	
}
