package ar.edu.utn.d2s.hibernate;

 
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import ar.edu.utn.d2s.helpers.PointOfInterestDummyFactory;
import ar.edu.utn.d2s.model.poi.Bus;
import ar.edu.utn.d2s.services.BusService;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BusDAOTest extends AbstractTest implements DAOTest{

	@Autowired
	private BusService busService;
	
	private Bus bus;
	
	@Override
	public void init() throws Exception {
		
		this.bus = PointOfInterestDummyFactory.createBus();
		 
	}

	
	@Override
	@Test	
	public void test_1_Insert() throws Exception {
		
		busService.save(bus);
		Assert.assertNotNull(bus);
		Assert.assertNotNull(busService.getById(bus.getId()).getId());
		busService.delete(bus);
		
	}

	@Override
	@Test	
	public void test_2_Update() throws Exception{
		
		busService.save(bus);
		bus.setName("A New Name");
		busService.update(bus);
		String name = busService.getById(bus.getId()).getName();
		Assert.assertEquals("A New Name", name);
		busService.delete(bus);
		
	}

	@Override
	@Test	
	public void test_3_Delete() throws Exception{
		
		busService.save(bus);
		Assert.assertNotNull(bus);
		Assert.assertNotNull(busService.getById(bus.getId()).getId());;
		busService.delete(bus);
		Bus deleted = busService.getById(bus.getId());
		Assert.assertNull(deleted);
	}

	@Override
	@Test	
	public void test_4_FindAll() throws Exception {
		busService.save(bus);
		Assert.assertEquals(1, busService.findAll().size());
		busService.delete(bus);
	}

	@Override
	@Test	
	public void test_5_FindByProperty() throws Exception{
		busService.save(bus);
		bus.setName("bus");
		busService.update(bus);
		Assert.assertEquals(1, busService.findByProperty("name", bus.getName()).size());	
		busService.delete(bus);
	}

	
}
