package ar.edu.utn.d2s.hibernate;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.junit.Test;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public interface DAOTest {
	
	@Test 
	void test_1_Insert() throws Exception;
	@Test 
	void test_2_Update() throws Exception;
	@Test 
    void test_3_Delete() throws Exception;
	@Test 
    void test_4_FindAll() throws Exception;
	@Test 
    void test_5_FindByProperty() throws Exception;
    
}
