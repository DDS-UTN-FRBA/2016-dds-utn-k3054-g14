package ar.edu.utn.d2s.hibernate;
 
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;  

import ar.edu.utn.d2s.helpers.UserDummyFactory;
import ar.edu.utn.d2s.model.User;
import ar.edu.utn.d2s.services.UserService;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserDAOTest extends AbstractTest implements DAOTest {

	@Autowired
	private UserService userService;
	
	private User user;
	private User terminal;
	
	
	@Override
	public void init(){
		
		this.user = UserDummyFactory.createAdmin();
		this.terminal = UserDummyFactory.createTerminal1();
		
	}
 
	@Override
	@Test	
	public void test_1_Insert() throws Exception {
		
		userService.save(user);
		Assert.assertNotNull(user);
		Assert.assertNotNull(userService.getById(user.getId()).getId());
		Assert.assertEquals("Administrator", user.getName()); 
		Assert.assertEquals(true, user.getSaveSearches());
		Assert.assertEquals(true, user.isAdmin());
		userService.delete(user);
		
	} 
	
	@Override
	@Test
	public void test_2_Update() throws Exception{
		
		userService.save(user);
		user.setName("New Name");
		userService.update(user);
		
		User userModified = userService.getById(user.getId());
				
		Assert.assertNotNull(userModified);
		Assert.assertNotNull(userService.getById(user.getId()).getId());
		Assert.assertEquals("New Name", userModified.getName()); 
		Assert.assertEquals(true, userModified.getSaveSearches());
		Assert.assertEquals(true, userModified.isAdmin());
		userService.delete(user);		
	}
	
	@Override
	@Test
	public void test_3_Delete() throws Exception{
		
		userService.save(user);
		Assert.assertNotNull(user);
		Assert.assertNotNull(userService.getById(user.getId()).getId());
		userService.delete(user);
		User deleted = userService.getById(user.getId());
		Assert.assertNull(deleted);
				
	}

	@Override
	@Test
	public void test_4_FindAll() throws Exception{
		
		userService.save(user);
		userService.save(terminal);		
		
		Assert.assertEquals(2, userService.findAll().size());
		userService.delete(user);
		userService.delete(terminal);
	}

	@Override
	@Test
	public void test_5_FindByProperty() throws Exception{
		
		userService.save(user);
		userService.save(terminal);
		user.setName("New Name");
		userService.update(user);
		
		Assert.assertEquals(1, userService.findByProperty("name", "New Name").size());		
		userService.delete(user);
		userService.delete(terminal);
	}

	
	
}
