package ar.edu.utn.d2s.hibernate;

import org.junit.After;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import ar.edu.utn.d2s.helpers.PointOfInterestDummyFactory;
import ar.edu.utn.d2s.model.poi.PointOfInterest;
import ar.edu.utn.d2s.services.PoiService;



@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PoiDAOTest extends AbstractTest implements DAOTest{
	
	
	@Autowired
	private PoiService poiService;
	
	private PointOfInterest cgp;
	private PointOfInterest bank;
	private PointOfInterest shop;
	private PointOfInterest bus;
	
	@Override
	public void init() throws Exception {
		
		 
		this.cgp = PointOfInterestDummyFactory.createCGP();
		this.bank = PointOfInterestDummyFactory.createBank();
		this.shop = PointOfInterestDummyFactory.createShop();
		this.bus = PointOfInterestDummyFactory.createBus();
	}

	@Override
	@After
    public void tearDown() throws Throwable {
    	 
	 
    }
	
	@Override
	@Test	
	public void test_1_Insert() throws Exception {
		
		poiService.save(cgp);
		poiService.save(bank);
		poiService.save(shop);
		poiService.save(bus);
		Assert.assertNotNull(poiService.getById(cgp.getId()).getId());  
		Assert.assertNotNull(poiService.getById(bank.getId()).getId()); 
		Assert.assertNotNull(poiService.getById(shop.getId()).getId()); 
		Assert.assertNotNull(poiService.getById(bus.getId()).getId()); 
		poiService.delete(cgp);
		poiService.delete(bank);
		poiService.delete(shop);
		poiService.delete(bus);
		
		
	}

	@Override
	@Test	
	public void test_2_Update() throws Exception{
		
		poiService.save(cgp);
		cgp.setName("A New  cgp Name");
		poiService.update(cgp);
		String name = poiService.getById(cgp.getId()).getName();
		Assert.assertEquals("A New  cgp Name", name); 
		
		poiService.save(bank);
		bank.setName("A New bank Name");
		poiService.update(bank);
		String name2 = poiService.getById(bank.getId()).getName();
		Assert.assertEquals("A New bank Name", name2); 
		
		poiService.save(shop);
		shop.setName("A New shop Name");
		poiService.update(shop);
		String name3 = poiService.getById(shop.getId()).getName();
		Assert.assertEquals("A New shop Name", name3); 
		
		poiService.save(bus);
		bus.setName("A New bus Name");
		poiService.update(bus);
		String name4 = poiService.getById(bus.getId()).getName();
		Assert.assertEquals("A New bus Name", name4); 
		
		poiService.delete(cgp);
		poiService.delete(bank);
		poiService.delete(shop);
		poiService.delete(bus);
		
	}

	@Override
	@Test	
	public void test_3_Delete() throws Exception{
		
		poiService.save(cgp);
		Assert.assertNotNull(cgp);
		Assert.assertNotNull(poiService.getById(cgp.getId()).getId()); 
		poiService.delete(cgp);
		PointOfInterest deleted = poiService.getById(cgp.getId());
		Assert.assertNull(deleted);
		
		poiService.save(bank);
		Assert.assertNotNull(bank);
		Assert.assertNotNull(poiService.getById(bank.getId()).getId()); 
		poiService.delete(bank);
		PointOfInterest deleted2 = poiService.getById(bank.getId());
		Assert.assertNull(deleted2);
		
		poiService.save(shop);
		Assert.assertNotNull(shop);
		Assert.assertNotNull(poiService.getById(shop.getId()).getId());
		poiService.delete(shop);
		PointOfInterest deleted3 = poiService.getById(shop.getId());
		Assert.assertNull(deleted3);
		
		poiService.save(bus);
		Assert.assertNotNull(bus);
		Assert.assertNotNull(poiService.getById(bus.getId()).getId());
		poiService.delete(bus);
		PointOfInterest deleted4 = poiService.getById(bus.getId());
		Assert.assertNull(deleted4);
	}

	@Override
	@Test	
	public void test_4_FindAll() throws Exception {

		poiService.save(cgp);
		poiService.save(bank);
		poiService.save(shop);
		poiService.save(bus);
		Assert.assertEquals(4, poiService.findAll().size());
		poiService.delete(cgp);
		poiService.delete(bank);
		poiService.delete(shop);
		poiService.delete(bus);
	}

	@Override
	@Test	
	public void test_5_FindByProperty() throws Exception{
		poiService.save(cgp);
		cgp.setName("A New Name 2");
		poiService.update(cgp);
		Assert.assertEquals(0, poiService.findByProperty("name", "A New").size());	
		poiService.delete(cgp);
	}
	
	
	
}

