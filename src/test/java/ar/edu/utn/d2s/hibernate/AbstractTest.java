package ar.edu.utn.d2s.hibernate;
 
import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;  
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;  

import ar.edu.utn.d2s.hibernate.HibernateConfiguration;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = HibernateConfiguration.class)
public abstract class AbstractTest {
	
	Session session;
	
	
    @Before
    public void setUp() throws Exception {
      
    	init();
    }

    @After
    public void tearDown() throws Throwable {
    	
    	    	
    }
    

	public abstract void init() throws Exception;


}
