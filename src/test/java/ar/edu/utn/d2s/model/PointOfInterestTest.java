package ar.edu.utn.d2s.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.joda.time.LocalTime;

import ar.edu.utn.d2s.exceptions.PointOfInterestFactoryException;
import ar.edu.utn.d2s.helpers.PointOfInterestDummyFactory;
import ar.edu.utn.d2s.libraries.days.Days;
import ar.edu.utn.d2s.model.Point;
import ar.edu.utn.d2s.model.poi.PointOfInterest;

public class PointOfInterestTest {
	
	private PointOfInterest bus;
	private PointOfInterest bank;
	private PointOfInterest cgp;
	private PointOfInterest shop;

	
	@Before
	public void setUp() throws Exception {
		this.bus = PointOfInterestDummyFactory.createBus();
		this.bank = PointOfInterestDummyFactory.createBank();
		this.cgp = PointOfInterestDummyFactory.createCGP();
		this.shop = PointOfInterestDummyFactory.createShop();
		
	}

	@Test(expected = PointOfInterestFactoryException.class)
	public void aPOICantHaveANameWithMoreThan60Characaters() throws PointOfInterestFactoryException
	{	
		this.bus.setName("Some string with a lot of words that exceed the 60 characters permitted");
	}
	
	@Test(expected = PointOfInterestFactoryException.class)  
	public void aPOICantHaveNullCoordinates() throws PointOfInterestFactoryException
	{	 
		this.bus.setCoordinates(null);
	}
	
	@Test(expected = PointOfInterestFactoryException.class)  
	public void aPOICantHaveAnEmptyName() throws PointOfInterestFactoryException
	{	
		this.cgp.setName("");
	}
	
	// Tests related to POI's proximity
	
	@Test
	public void testProximityOfABusStop()
	{
		Point squareFlores = new Point(-34.628848, -58.462722);
		Point burgerKing = new Point(-34.625758, -58.454407);
		
		assertTrue(bus.isCloseTo(squareFlores));
		assertFalse(bus.isCloseTo(burgerKing));
	}
	
	@Test 
	public void testProximityOfAShop()
	{
		Point obelisco = new Point(-34.603719, -58.381633);
		Point CilindroOfAvellaneda = new Point(-34.663632, -58.363022);

		assertTrue(shop.isCloseTo(obelisco));
		assertFalse(shop.isCloseTo(CilindroOfAvellaneda));
	}
	
	@Test
	public void testProximityOfACGP()
	{
		Point elCuartitoPizza = new Point(-34.597829, -58.385537);
		Point RetiroStation = new Point(-34.591153, -57.374260);

		assertTrue(cgp.isCloseTo(elCuartitoPizza));
		assertFalse(cgp.isCloseTo(RetiroStation));
	}
	
	@Test
	public void testProximityOfABank()
	{
		Point squareFlores = new Point(-34.628848, -58.462722);
		Point burgerKing = new Point(-34.625758, -58.454407);

		assertTrue(bank.isCloseTo(squareFlores));
		assertFalse(bank.isCloseTo(burgerKing));
	}
	
	// Tests related to POI's availability
	
	@Test
	public void aBusIsAlwaysAvailable()
	{
		assertTrue(bus.isAvailable(Days.getMonday(), new LocalTime(10,00,00), "aService"));
		assertTrue(bus.isAvailable(Days.getTuesday(), new LocalTime(20,20,00), "otherService"));
		assertTrue(bus.isAvailable(Days.getWednesday(), new LocalTime(15,00,00), "anyService"));
		assertTrue(bus.isAvailable(Days.getThursday(), new LocalTime(9,50,00), "newService"));
		assertTrue(bus.isAvailable(Days.getFriday(), new LocalTime(3,40,00), "aService"));
		assertTrue(bus.isAvailable(Days.getSaturday(), new LocalTime(1,1,1), "otherService"));
	}
	
	@Test
	public void testCGPAvailability()
	{
		assertTrue(cgp.isAvailable(Days.getMonday(), new LocalTime(10,00,00), "Rentas"));
		assertFalse(cgp.isAvailable(Days.getWednesday(), new LocalTime(16,00,00), "Rentas"));
		assertTrue(cgp.isAvailable(Days.getThursday(), new LocalTime(12,30,00)));
		assertTrue(cgp.isAvailable(Days.getThursday(), new LocalTime(12,30,00)));
		assertFalse(cgp.isAvailable(Days.getMonday(), new LocalTime(8,00,00)));
	}
 
	@Test
	public void testBankAvailability()
	{
		assertTrue(bank.isAvailable(Days.getTuesday(), new LocalTime(11,00,00), "Atencion Al Cliente"));
		assertTrue(bank.isAvailable(Days.getTuesday(), new LocalTime(11,00,00)));
		assertFalse(bank.isAvailable(Days.getTuesday(), new LocalTime(6,00,00)));
	}

	@Test
	public void testShopAvailability()
	{
		assertTrue(shop.isAvailable(Days.getWednesday(), new LocalTime(18,00,00)));
		assertFalse(shop.isAvailable(Days.getWednesday(), new LocalTime(15,00,00)));
	}
	
	// Tests related to searching of POIs

	@Test 
	public void testShopSearchingStrategy()
	{
		// by shop's name
		assertTrue(shop.matches("Carrousel"));
		
		// by keywords
		assertTrue(shop.matches("Uniformes escolares"));
		assertTrue(shop.matches("Comuniones"));
		assertTrue(shop.matches("Baile"));
		assertTrue(shop.matches("Ropa para bebe"));
		assertFalse(shop.matches("Deportes"));
		assertFalse(shop.matches("Comida"));
		assertFalse(shop.matches("Turismo"));
		
		// by category name
		assertTrue(shop.matches("Indumentaria Infantil"));
		assertFalse(shop.matches("Solo Deportes"));
	}
	
	@Test
	public void testCGPSearchingStrategy()
	{
		// by service's description
		assertTrue(cgp.matches("Rent"));
		
		// by commune number
		assertTrue(cgp.matches("1"));
		assertFalse(cgp.matches("3"));
	}
	
	@Test
	public void testBusSearchingStrategy()
	{
		// by line number
		assertTrue(bus.matches("132"));
		assertFalse(bus.matches("140"));
	}
	
	@Test
	public void testBankSearchingStrategy()
	{
		// by service's description
		assertFalse(bank.matches("Atencion"));
		
		// by bank's name
		assertTrue(bank.matches("Cajero Banco de"));
		assertFalse(bank.matches("Banco Santander Rio"));
	}
	
	@Test 
	public void testComparisonBetweenPointsOfInterest() throws PointOfInterestFactoryException
	{	
		assertTrue(this.bus.is(this.bus));
		assertFalse(this.bus.is(this.cgp));
	}
}
